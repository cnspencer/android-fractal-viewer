package com.charlestheman.basicfractal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String PREF_MAX_ITERATIONS = "pref_max_iterations";
    public static final String PREF_COLOR_SCHEME = "pref_color_schemes";
    public static final String PREF_ALLOW_ROTATION = "pref_allow_rotation";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        Preference connectionPref = findPreference(PREF_MAX_ITERATIONS);
        connectionPref.setSummary("Max iterations: " + sharedPreferences.getString(PREF_MAX_ITERATIONS, ""));

        connectionPref = findPreference(PREF_COLOR_SCHEME);
        connectionPref.setSummary("Color Scheme: " + sharedPreferences.getString(PREF_COLOR_SCHEME, ""));

        connectionPref = findPreference(PREF_ALLOW_ROTATION);
        connectionPref.setSummary("Allow rotation: " + sharedPreferences.getBoolean(PREF_ALLOW_ROTATION, true));
    }

    @Override
    public void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this.getActivity()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this.getActivity()).unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(PREF_MAX_ITERATIONS)) {
            Preference connectionPref = findPreference(key);
            connectionPref.setSummary("Max iterations: " + sharedPreferences.getString(key, ""));
        } else if (key.equals(PREF_COLOR_SCHEME)) {
            Preference connectionPref = findPreference(key);
            connectionPref.setSummary("Color Scheme: " + sharedPreferences.getString(PREF_COLOR_SCHEME, ""));
        } else if (key.equals(PREF_ALLOW_ROTATION)) {
            Preference connectionPref = findPreference(key);
            connectionPref.setSummary("Allow rotation: " + sharedPreferences.getBoolean(PREF_ALLOW_ROTATION, true));
        }
    }
}
