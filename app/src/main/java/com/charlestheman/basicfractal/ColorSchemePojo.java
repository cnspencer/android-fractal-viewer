package com.charlestheman.basicfractal;

import java.util.ArrayList;

/**
 * Holds all the data to be serialized for a color scheme
 */
public class ColorSchemePojo {
    public static final int CURRENT_VERSION = 1;

    private int version = CURRENT_VERSION;
    private ArrayList<Integer> positions;
    private ArrayList<float[]> hsvComponents;

    private boolean allowSplines = true;
    private int ordinal;
    private int refCount; // favorites can reference this

    public ArrayList<Integer> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<Integer> positions) {
        this.positions = positions;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public ArrayList<float[]> getHsvComponents() {
        return hsvComponents;
    }

    public void setHsvComponents(ArrayList<float[]> hsvComponents) {
        this.hsvComponents = hsvComponents;
    }

    public boolean isAllowSplines() {
        return allowSplines;
    }

    public void setAllowSplines(boolean allowSplines) {
        this.allowSplines = allowSplines;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public int getRefCount() {
        return refCount;
    }

    public void setRefCount(int refCount) {
        this.refCount = refCount;
    }
}
