package com.charlestheman.basicfractal;

import java.util.List;

/**
 * Represents classes that support the creation of CalculationIterable lists
 */
public interface CalculationIterable<CalcInfo, Result> {
    /**
     * Generates num number of CalculationIterators and returns them in a list
     *
     * @param num The number of CalculationIterators to generate
     * @return A list of the CalculationIterators, whose sets of elements over which they iterate
     * must be mutually exclusive and exhaustive for all the elements in this class
     */
    List<CalculationIterator<CalcInfo, Result>> iterators(int num);
}
