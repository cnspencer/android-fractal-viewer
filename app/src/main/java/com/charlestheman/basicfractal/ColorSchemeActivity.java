package com.charlestheman.basicfractal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Arrays;

import static android.widget.LinearLayout.LayoutParams;

public class ColorSchemeActivity extends Activity implements AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener {
    public static final int PICK_NEW_COLOR = 1;
    public static final int PICK_EDIT_COLOR = 2;

    private static final int HIGHLIGHT_COLOR = 0xFF00D0FF;
    private static final int DEFAULT_COLOR = 0xFF000000;
    private static final int TRANSPARENT_COLOR = 0x00000000;

    public enum Params {
        COLOR_SCHEME_TO_EDIT("colorSchemeToEdit");

        private String name;
        public String getName() { return name; }
        Params(String name) { this.name = name; }
    }

    private ColorScheme colorScheme = null;
    private ColorAdapter adapter;
    private int positionEditing = 0;

    private ActionMode actionMode = null;
    private int selectedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_scheme);

        String colorSchemeName = this.getIntent().getStringExtra(Params.COLOR_SCHEME_TO_EDIT.getName());
        ColorSchemeManager manager = new ColorSchemeManager(getBaseContext());
        if (colorSchemeName == null) {
            colorScheme = manager.createColorScheme();
        } else {
            colorScheme = manager.retrieveColorScheme(colorSchemeName);
            if (colorScheme == null) {
                colorScheme = manager.createColorScheme();
            }
        }
        if (colorScheme == null) {
            Intent data = new Intent();
            setResult(RESULT_CANCELED, data);
            finish();
        }

        // initialize color scheme if no colors
        if (colorScheme.size() == 0) {
            colorScheme.addColor(0xFFF0F0FF); // TODO hard-coded
            colorScheme.addColor(0xFF0000FF);
        }

        final ImageButton addButton = (ImageButton) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionMode != null) {
                    return;
                }
                if (colorScheme.size() < ColorScheme.MAX_COLORS) {
                    Intent i = new Intent(ColorSchemeActivity.this, ColorPickerActivity.class);
                    i.putExtra(ColorPickerActivity.Params.NEW_COLOR.getName(), true);
                    startActivityForResult(i, PICK_NEW_COLOR);
                } else {
                    Toast.makeText(getBaseContext(),
                            "The maximum number of colors (" + ColorScheme.MAX_COLORS + ") have been added.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        ListView colorsView = (ListView) findViewById(R.id.colorsView);
        adapter = new ColorAdapter(this);
        colorsView.setAdapter(adapter);
        colorsView.setOnItemClickListener(this);
        colorsView.setOnItemLongClickListener(this);

        View view = findViewById(R.id.gradientView);
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                refreshGradientView();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_NEW_COLOR) {
            if (resultCode == RESULT_OK) {
                int color = data.getIntExtra(ColorPickerActivity.Results.COLOR.getName(), DEFAULT_COLOR);
                colorScheme.addColor(color);
                refreshGradientView();
                adapter.notifyDataSetChanged();
            }
        } else if (requestCode == PICK_EDIT_COLOR) {
            if (resultCode == RESULT_OK) {
                int color = data.getIntExtra(ColorPickerActivity.Results.COLOR.getName(), DEFAULT_COLOR);
                colorScheme.setColor(positionEditing, color);
                refreshGradientView();
                adapter.notifyDataSetChanged();
            }
        }
    }

    private int convertDPToPixels(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    public View getColorView(int color) {
        LinearLayout outer = new LinearLayout(this);
        // TODO rename
        int outerHeight = convertDPToPixels(4); // TODO
        int outerWidth = convertDPToPixels(4); // TODO
        outer.setPadding(outerHeight, outerHeight, outerHeight, outerHeight);

        View view = new View(this);
        view.setBackgroundColor(color);

        int height = convertDPToPixels(64); // TODO
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, height);
        view.setLayoutParams(layoutParams);

        outer.addView(view);
        return outer;
    }

    public void swapColors(int position1, int position2) {
        int temp = colorScheme.getColor(position2);
        colorScheme.setColor(position2, colorScheme.getColor(position1));
        colorScheme.setColor(position1, temp);
        refreshGradientView();
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        adapter.notifyDataSetChanged();
    }

    private void refreshGradientView() {
        GradientView view = (GradientView)findViewById(R.id.gradientView);
        view.setColorScheme(colorScheme);
        view.postInvalidate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.color_scheme_context, menu);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (selectedPosition < 0) {
            Intent i = new Intent(ColorSchemeActivity.this, ColorPickerActivity.class);
            i.putExtra(ColorPickerActivity.Params.NEW_COLOR.getName(), false);
            i.putExtra(ColorPickerActivity.Params.OLD_COLOR.getName(), colorScheme.getColor(position));
            startActivityForResult(i, PICK_EDIT_COLOR);
            positionEditing = position;
        } else {
            swapColors(selectedPosition, position);
            setSelectedPosition(position);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (actionMode != null) {
            return false;
        }

        selectedPosition = position;
        adapter.notifyDataSetChanged();

        ListView colorsView = (ListView) findViewById(R.id.colorsView);
        colorsView.setSelection(selectedPosition);

        actionMode = startActionMode(new SelectedActionModeCallback());
        view.setSelected(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.color_scheme_cancel: {
                Intent data = new Intent();
                setResult(RESULT_CANCELED, data);
                finish();
                return true;
            }
            case R.id.color_scheme_ok: {
                colorScheme.save();
                Intent data = new Intent();
                setResult(RESULT_OK, data);
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    class SelectedActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.color_scheme_selected_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {

                case R.id.color_scheme_menu_move_up:
                    if (selectedPosition > 0) {
                        swapColors(selectedPosition, selectedPosition - 1);
                        setSelectedPosition(selectedPosition - 1);
                    }
                    return true;

                case R.id.color_scheme_menu_move_down:
                    if (selectedPosition < colorScheme.size() - 1) {
                        swapColors(selectedPosition, selectedPosition + 1);
                        setSelectedPosition(selectedPosition + 1);
                    }
                    return true;

                case R.id.color_scheme_menu_delete:
                    colorScheme.removeColor(selectedPosition);
                    refreshGradientView();
                    adapter.notifyDataSetChanged();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
            selectedPosition = -1;
            adapter.notifyDataSetChanged();
        }
    }

    public class ColorAdapter extends BaseAdapter {

        public ColorAdapter(Context c) {
        }

        @Override
        public int getCount() {
            return colorScheme == null ? 0 : colorScheme.size();
        }

        @Override
        public Object getItem(int position) {
            return colorScheme == null ? null : colorScheme.getColor(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView instanceof LinearLayout) {
                view = convertView;
                LinearLayout outer = (LinearLayout) convertView;
                if (outer.getChildCount() > 0) {
                    View child = outer.getChildAt(0);
                    child.setBackgroundColor(colorScheme.getColor(position));
                }
            } else {
                view = getColorView(colorScheme.getColor(position));
            }
            view.setBackgroundColor(selectedPosition == position ? HIGHLIGHT_COLOR : TRANSPARENT_COLOR);
            return view;
        }
    }
}
