package com.charlestheman.basicfractal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ColorSchemeListActivity extends Activity implements AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener {
    private static final int HIGHLIGHT_COLOR = 0xFF00D0FF;
    private static final int TRANSPARENT_COLOR = 0x00000000;

    private static final int CREATE_NEW_COLOR_SCHEME = 1;
    private static final int EDIT_COLOR_SCHEME = 2;

    public enum Results {
        COLOR_SCHEME_PATH("colorSchemePath"),
        COLOR_SCHEME_NAME("colorSchemeName"); // for defaults which have no path

        private String name;
        public String getName() { return name; }
        Results(String name) { this.name = name; }
    }

    public enum DefaultColorSchemes {
        MOONLIT_NIGHT("Moonlit Night", 0xff7600ff, 0xff002534, 0xff30007f, 0xffffeeff, 0xff390053, 0xff7200ff),
        FLAG("Flag", 0xffff1a00, 0xff9c0700, 0xffffe5e5, 0xffede3ff, 0xff1400ff, 0xff050083, 0xffe5e0ff, 0xffffdadf, 0xffff2700),
        PINK_PURPLE("Pink and Purple", 0xffffddf6, 0xff320039, 0xffff007a, 0xffffeef2),
        SILVERY("Silvery", 0xffddeeff, 0xff362f42, 0xff020060, 0xffe7fdfc),
        KALEIDOSCOPE("Kaleidoscope", 0xffff0a00, 0xff34002f, 0xffeff0ff, 0xff00ffec, 0xff062800, 0xffffffef, 0xffff1400),
        SUNSET_SUNRISE("Sunset and Sunrise", 0xfffffff1, 0xff080025, 0xffedfff0),
        VIBRANT("Vibrant", 0xffff0011, 0xff0018ff, 0xff38ff00, 0xffff0001);

        private String name;
        private ColorScheme colorScheme = new ColorScheme();

        public String getName() { return name; }
        public ColorScheme getColorScheme() { return colorScheme; }
        DefaultColorSchemes(String name, int... colors) {
            this.name = name;
            this.colorScheme.setReadOnly(true);
            this.colorScheme.setName(name);
            for (int color : colors) {
                colorScheme.addColor(color);
            }
        }

        public static DefaultColorSchemes getByName(String name) {
            for (DefaultColorSchemes def : values()) {
                if (def.name.equals(name)) {
                    return def;
                }
            }
            return null;
        }
    }

    private ColorSchemeManager manager = null;
    private List<ColorScheme> colorSchemes = new ArrayList<>();

    private ListView colorSchemesView;
    private ColorSchemeAdapter adapter;
    private ActionMode actionMode = null;
    private int selectedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_scheme_list);
        manager = new ColorSchemeManager(getBaseContext());

        colorSchemesView = (ListView) findViewById(R.id.colorSchemesView);
        adapter = new ColorSchemeAdapter(this);
        colorSchemesView.setAdapter(adapter);
        colorSchemesView.setOnItemClickListener(this);
        colorSchemesView.setOnItemLongClickListener(this);

        final ImageButton addButton = (ImageButton) findViewById(R.id.addColorSchemeButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionMode != null) {
                    return;
                }
                if (manager.canAddColorScheme()) {
                    Intent i = new Intent(ColorSchemeListActivity.this, ColorSchemeActivity.class);
                    startActivityForResult(i, CREATE_NEW_COLOR_SCHEME);
                } else {
                    Toast.makeText(getBaseContext(),
                            "The maximum number of color schemes (" + ColorSchemeManager.MAX_COLOR_SCHEMES + ") has been reached.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadColorSchemes();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CREATE_NEW_COLOR_SCHEME || requestCode == EDIT_COLOR_SCHEME) {
            if (resultCode == RESULT_OK) {
                loadColorSchemes();
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void loadColorSchemes() {
        colorSchemes = manager.getAllColorSchemes(); // TODO inefficient
        for (DefaultColorSchemes def : DefaultColorSchemes.values()) {
            colorSchemes.add(def.colorScheme);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (selectedPosition < 0) {
            Intent data = new Intent();
            ColorScheme colorScheme = colorSchemes.get(position);
            if (colorScheme.getDirectory() != null) {
                data.putExtra(Results.COLOR_SCHEME_PATH.getName(), colorScheme.getDirectory().getAbsolutePath());
            } else {
                data.putExtra(Results.COLOR_SCHEME_NAME.getName(), colorScheme.getName());
            }
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (selectedPosition < 0 && !colorSchemes.get(position).isReadOnly()) {
            selectedPosition = position;
            adapter.notifyDataSetChanged();

            ListView colorSchemesView = (ListView) findViewById(R.id.colorSchemesView);
            colorSchemesView.setSelection(selectedPosition);

            actionMode = startActionMode(new SelectedActionModeCallback());
            view.setSelected(true);
            return true;
        } else {
            return true;
        }
    }

    public class SelectedActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.color_scheme_list_selected_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {

                case R.id.color_scheme_list_menu_edit:
                    Intent i = new Intent(ColorSchemeListActivity.this, ColorSchemeActivity.class);
                    i.putExtra(ColorSchemeActivity.Params.COLOR_SCHEME_TO_EDIT.getName(),
                            colorSchemes.get(selectedPosition).getDirectory().getName());
                    startActivityForResult(i, EDIT_COLOR_SCHEME);
                    mode.finish();
                    return true;

                case R.id.color_scheme_list_menu_delete:
                    // ask the user if they are sure
                    AlertDialog.Builder builder = new AlertDialog.Builder(ColorSchemeListActivity.this);
                    builder.setMessage("Are you sure you want to delete this color scheme?"); // TODO
                    builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                colorSchemes.get(selectedPosition).delete();
                                loadColorSchemes();
                            } catch (IOException e) {
                                Log.e(ColorSchemeListActivity.class.getName(), "Failed to delete color scheme", e);
                            }
                            adapter.notifyDataSetChanged();
                            actionMode.finish();
                        }
                    });
                    builder.setNegativeButton("Cancel",  new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            adapter.notifyDataSetChanged();
                            actionMode.finish();
                        }
                    });
                    builder.show();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            selectedPosition = -1;
            actionMode = null;
            adapter.notifyDataSetChanged();
        }
    }

    public class ColorSchemeAdapter extends BaseAdapter {
        public ColorSchemeAdapter(Context c) {
        }

        @Override
        public int getCount() {
            return colorSchemes.size();
        }

        @Override
        public Object getItem(int position) {
            return colorSchemes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.color_scheme_list_gradient_view_horizontal,
                    parent, false);
            view.setBackgroundColor(selectedPosition == position ? HIGHLIGHT_COLOR : TRANSPARENT_COLOR);

            GradientView gradientView = (GradientView) view.findViewById(R.id.gradientView);
            gradientView.setColorScheme(colorSchemes.get(position));
            return view;
        }
    }
}
