package com.charlestheman.basicfractal;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages operations related to favorites
 */
public class FavoritesManager {
    private static final int MAX_FAVORITES = 500;
    private static final String FAVORITES_DIRECTORY = "favorites";

    private final Context baseContext;
    private File favoritesDirectory = null;

    public FavoritesManager(Context baseContext) {
        this.baseContext = baseContext;
    }

    private File getFavoritesDir() {
        if (favoritesDirectory == null) {
            File internalDir = baseContext.getFilesDir();
            String favoritesPathname = internalDir.getAbsolutePath() +
                    File.separator + FAVORITES_DIRECTORY + File.separator;
            File newFavoritesDir = new File(favoritesPathname);

            if (!newFavoritesDir.exists()) {
                boolean result = newFavoritesDir.mkdir();
                if (!result) {
                    Log.d("Error", "Favorites directory can't be created!");
                } else {
                    favoritesDirectory = newFavoritesDir;
                }
            } else {
                favoritesDirectory = newFavoritesDir;
            }
        }

        return favoritesDirectory;
    }

    public List<Favorite> getAllFavorites() {
        List<Favorite> favorites = new ArrayList<>();
        for (File file : getFavoritesDir().listFiles()) {
            // assuming that the file for a favorite
            if (file.isDirectory()) {
                favorites.add(new Favorite(file));
            }
        }
        return favorites;
    }

    private File getNextFavoriteDir() {
        File favoritesDir = getFavoritesDir();
        if (favoritesDir == null) return null;

        File[] subDirs = favoritesDir.listFiles();

        outer:
        for (int i = 1; i <= MAX_FAVORITES; ++i) {
            for (File file : subDirs) {
                if (file.isDirectory() && file.getName().equals("favorite" + i)) {
                    continue outer; // try the next name
                }
            }
            return new File(favoritesDir.getAbsolutePath() + File.separator + "favorite" + i);
        }
        return null;
    }

    public Favorite createFavorite() {
        File favoriteDir = getNextFavoriteDir();
        if (favoriteDir == null) {
            return null;
        }
        return new Favorite(favoriteDir);
    }
}
