package com.charlestheman.basicfractal;

/**
 * Contiains a string to ColoringStrategy mapping for all the built in old coloring strategies
 */
public enum ColoringStrategies {
    OCEAN("Ocean", new BasicTrigColoring(
            ((2d * 50d) / 70d) * Math.PI,
            ((2d * 60d) / 70d) * Math.PI,
            (2d / 70d) * Math.PI)),
    ALIEN("Alien", new BasicTrigColoring(
            ((2d * 50d) / 70d) * Math.PI,
            (2d / 70d) * Math.PI,
            ((2d * 60d) / 70d) * Math.PI)),
    SUNSET("Sunset", new BasicTrigColoring(
            (2d / 70d) * Math.PI,
            ((2d * 60d) / 70d) * Math.PI,
            ((2d * 50d) / 70d) * Math.PI)),
    MOTLEY("Motley", new BasicTrigColoring(
            ((2d * 50d) / 70d) * Math.PI,
            ((2d * 60d) / 70d) * Math.PI,
            (2d / 70d) * Math.PI,
            2.2d,
            0.475d,
            1.782d)),
    GRAYSCALE("Grayscale", new BasicTrigColoring(
            (2d / 70d) * Math.PI,
            (2d / 70d) * Math.PI,
            (2d / 70d) * Math.PI)),
    CUSTOM("Custom", null);

    private final String name;
    private final ColoringStrategy cs;

    ColoringStrategies(String name, ColoringStrategy cs) {
        this.name = name;
        this.cs = cs;
    }

    public String getName() {
        return name;
    }
    public ColoringStrategy getColoringStrategy() {
        return cs;
    }

    public static ColoringStrategy getByName(String name) {
        for (ColoringStrategies csm : ColoringStrategies.values()) {
            if (csm.name.equals(name)) {
                return csm.cs;
            }
        }
        return null;
    }

    public static String getNameFromColoringStrategy(ColoringStrategy cs) {
        for (ColoringStrategies csm : ColoringStrategies.values()) {
            if (csm != CUSTOM && csm.cs.equals(cs)) {
                return csm.name;
            }
        }
        return CUSTOM.name;
    }

    public static ColoringStrategy getDefault() {
        return OCEAN.cs;
    }
}
