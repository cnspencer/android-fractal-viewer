package com.charlestheman.basicfractal;

import java.util.concurrent.Callable;

/**
 * Implements a Runnable to simply calculate a set of pixels for a fractal using CalculationIterator
 */
public class CalculationTask implements Callable<Void> {
    private final CalculationIterator<SampleManager.CalcInfo, SampleManager.Result> iterator;
    private final ColoringStrategy coloringStrategy;
    private boolean cancelled = false;
    private float[] hsvBuffer = new float[3];

    public CalculationTask(CalculationIterator<SampleManager.CalcInfo, SampleManager.Result> iterator,
                           ColoringStrategy coloringStrategy) {
        this.iterator = iterator;
        this.coloringStrategy = coloringStrategy;
    }

    public Void call() {
        SampleManager.CalcInfo calcInfo = new SampleManager.CalcInfo();
        SampleManager.Result result = new SampleManager.Result();

        if (cancelled) {
            if (iterator.hasNext()) {
                iterator.next(calcInfo);
                result.cancelled = true;
                result.color = 0x00000000;
                iterator.set(result);
            }
            return null;
        }

        while (iterator.hasNext()) {
            iterator.next(calcInfo);
            result.color = coloringStrategy.getColor(calcInfo.x, calcInfo.y, hsvBuffer);
            iterator.set(result);
        }

        return null;
    }

    public void cancel() {
        cancelled = true;
    }
}
