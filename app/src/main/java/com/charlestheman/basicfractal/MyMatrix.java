package com.charlestheman.basicfractal;

import javax.vecmath.Matrix3d;

/**
 * This is a subclass of Matrix3d to help me do some common matrix calculations easily.
 */
public class MyMatrix extends Matrix3d {
    private final Matrix3d workingMatrix = new Matrix3d();

    /**
     * This does a translation of T by this matrix M.
     * M' = T(x, y) * M
     *
     * @param x translation x
     * @param y translation y
     */
    public void translate(double x, double y) {
        workingMatrix.setIdentity();
        workingMatrix.m02 = x;
        workingMatrix.m12 = y;

        this.mul(workingMatrix, this);
    }

    /**
     * This does a scaling by the inverse of the given scale factor
     * M' = S(1/scaleFactor) * M
     *
     * @param scaleFactor scale factor, which will be inverted
     */
    public void scale(double scaleFactor) {
        workingMatrix.setIdentity();
        double inverseScaleFactor = 1d / scaleFactor;
        workingMatrix.m00 = inverseScaleFactor;
        workingMatrix.m11 = inverseScaleFactor;

        workingMatrix.m00 = inverseScaleFactor;
        workingMatrix.m11 = inverseScaleFactor;

        this.mul(workingMatrix, this);
    }



    /**
     * This does a rotation based on angle, where positive angle is clockwise
     * M' = R(angle) * M
     *
     * @param angle angle of clockwise rotation
     */
    public void rotate(double angle) {
        double cosine = Math.cos(angle);
        double sine = Math.sin(angle);
        workingMatrix.setIdentity();
        workingMatrix.m00 = cosine;
        workingMatrix.m01 = -sine;
        workingMatrix.m10 = sine;
        workingMatrix.m11 = cosine;

        this.mul(workingMatrix, this);
    }

    /**
     * Reflects about the X axis
     *
     * M' = Ref(X-axis) * M
     */
    public void reflectXAxis() {
        workingMatrix.setIdentity();
        workingMatrix.m11 = -workingMatrix.m11;
        this.mul(workingMatrix, this);
    }

    /**
     * Reflects about the Y axis
     *
     * M' = Ref(y-axis) * M
     */
    public void reflectYAxis() {
        workingMatrix.setIdentity();
        workingMatrix.m00 = -workingMatrix.m00;
        this.mul(workingMatrix, this);
    }
}
