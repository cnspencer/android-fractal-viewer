package com.charlestheman.basicfractal;

import java.io.Serializable;

/**
 * Interface for an iteration-based fractal algorithm, using the strategy pattern
 * This class is immutable
 */
public interface IterationStrategy extends Serializable {
    /**
     * Gets the number of escape iterations
     *
     * @param x0 X coordinate
     * @param y0 Y coordinate
     * @return Number of iterations, ranging from 0 to a maximum defined by
     * setMaximumEscapeIterations()
     */
    int getEscapeIterations(final double x0, final double y0);

    /**
     * Gets the maximum value that can be returned from getEscapeIterations(double, double)
     *
     * @return The maximum escape iterations that can be returned before bailing out
     */
    int getMaximumEscapeIterations();
}

