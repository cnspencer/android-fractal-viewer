package com.charlestheman.basicfractal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;


public class FavoritesActivity extends Activity implements AdapterView.OnItemClickListener {
    private static final int RESULT_SELECTED_FAVORITE = 1;
    private static final int HIGHLIGHT_COLOR = 0x7F007FFF;

    private GridView gridView = null;
    private List<Favorite> favorites = null;
    private Favorite selectedFavorite = null;

    private ImageView selectedImageView = null;

    private FavoritesManager favoritesManager = null;

    private ActionMode actionMode = null;
    private final ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.favorites_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.favorites_context_menu_delete:
                    Toast toast = Toast.makeText(FavoritesActivity.this.getApplicationContext(), "Deleting favorite...", Toast.LENGTH_LONG);
                    toast.show();

                    AsyncTask<Favorite, Void, Boolean> task = new AsyncTask<Favorite, Void, Boolean>() {
                        @Override
                        protected Boolean doInBackground(Favorite... params) {
                            try {
                                params[0].delete();
                            } catch(IOException e) {
                                Log.e(FavoritesActivity.class.getName(), "Error deleting favorite", e);
                                return false;
                            }
                            return true;
                        }

                        @Override
                        protected void onPostExecute(Boolean result) {
                            if (result) {
                                Toast.makeText(FavoritesActivity.this.getApplicationContext(), "Delete complete.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(FavoritesActivity.this.getApplicationContext(), "Delete failed!", Toast.LENGTH_SHORT).show();
                            }
                            FavoritesActivity.this.favorites = favoritesManager.getAllFavorites();
                            gridView.setAdapter(new ImageAdapter(FavoritesActivity.this));
                        }
                    };

                    task.execute(selectedFavorite);
                    mode.finish();

                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if (FavoritesActivity.this.selectedImageView != null) {
                FavoritesActivity.this.selectedImageView.setColorFilter(Color.TRANSPARENT);
            }
            actionMode = null;
            selectedFavorite = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites_gridview);

        favoritesManager = new FavoritesManager(this.getBaseContext());

        this.favorites = favoritesManager.getAllFavorites();

        // get handles to the FractalView from XML
        this.gridView = (GridView) findViewById(R.id.favorites_gridview);

        gridView.setAdapter(new ImageAdapter(this));

        gridView.setOnItemClickListener(this);
        gridView.setFocusable(true);

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (actionMode != null) {
                    return false;
                }

                FavoritesActivity.this.selectedImageView = (ImageView) view;
                FavoritesActivity.this.selectedImageView.setColorFilter(HIGHLIGHT_COLOR);

                selectedFavorite = favorites.get(position);
                actionMode = FavoritesActivity.this.startActionMode(actionModeCallback);
                view.setSelected(true);
                return true;
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent data = new Intent();
        data.putExtra("favoriteDirectory", favorites.get(position).getDirectory().getAbsolutePath());

        this.setResult(RESULT_SELECTED_FAVORITE, data);
        this.finish();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public class ImageAdapter extends BaseAdapter {
        private final List<Favorite> favorites;
        private Context context = null;

        public ImageAdapter(Context c) {
            this.context = c;
            this.favorites = favoritesManager.getAllFavorites();
        }

        @Override
        public int getCount() {
            return favorites.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;

            // I've noticed that sometimes the first one or two ImageViews have to come back in as convertView
            // since they were made before the GridView was completely set up (e.g. gridView width was 0).
            // Also, sometimes the ImageView's won't repaint if I try to re-use the convertView, so I just
            // always give them a new one.

            imageView = new ImageView(this.context);

            int pixelWidth = gridView.getWidth() / gridView.getNumColumns();
            imageView.setLayoutParams(new GridView.LayoutParams(pixelWidth, pixelWidth));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(0, 0, 0, 0);

            Bitmap thumbnail = favorites.get(position).getThumbnail();
            if (thumbnail != null) {
                imageView.setImageBitmap(thumbnail);
            }
            return imageView;
        }
    }
}
