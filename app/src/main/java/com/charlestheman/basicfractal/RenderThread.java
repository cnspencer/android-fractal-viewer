package com.charlestheman.basicfractal;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.os.Process;

public class RenderThread extends Thread {
    private static final int TARGET_FPS = 30;
    private final Object syncObject = new Object();
    // Thread control
    private boolean running = false;
    private long lastTick = 0;
    // Stuff we need to reference
    private SurfaceHolder surfaceHolder = null;
    private RenderingManager renderingManager = null;
    private FractalController fractalController = null;
    private Paint activeImagePaint = null;
    private Rect activeImageDestRect = null;
    private Bitmap lastBitmap = null;
    private Matrix lastMatrix = null;
    private Matrix adjustmentMatrix = new Matrix();

    public RenderThread(SurfaceHolder surfaceHolder, FractalView fractalView, RenderingManager renderingManager,
                        FractalController fractalController) {
        this.surfaceHolder = surfaceHolder;
        this.activeImagePaint = new Paint();
        this.activeImagePaint.setFilterBitmap(true);
        this.activeImagePaint.setAntiAlias(true);
        this.activeImagePaint.setDither(true);
        this.activeImageDestRect = new Rect(0, 0, fractalView.getWidth(), fractalView.getHeight());
        this.renderingManager = renderingManager;
        this.fractalController = fractalController;
    }

    public Bitmap getLastBitmap() {
        // Note that this technically could give an old bitmap, but that's very unlikely
        return lastBitmap;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * Get the matrix used for scaling/dragging adjustments
     *
     * @return The matrix used for scaling/dragging adjustments
     */
    public Matrix getAdjustmentMatrix() {
        return this.adjustmentMatrix;
    }

    public void recalculate() {
        this.fractalController.postChange();

        lastMatrix = adjustmentMatrix;
        this.adjustmentMatrix = new Matrix();
    }

    public void restart() {
        this.fractalController.resetPosition();
        recalculate();
    }

    @Override
    public void run() {
//        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        int waitTime = 1000 / TARGET_FPS;

        long current;

        while (running) {
            fractalController.tick();

            current = System.currentTimeMillis();

            if (current - lastTick >= waitTime) {
                lastTick = current;
                Canvas canvas = null;
                try {
                    canvas = surfaceHolder.lockCanvas();
                    synchronized (syncObject) {
                        if (canvas != null) {
                            canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                            canvas.drawColor(renderingManager.getLastAverageEdgeColor());
                            Bitmap bitmap = renderingManager.getCurrentBitmap();


                            if (bitmap == null) {
                                if (lastMatrix != null && lastBitmap != null) {
                                    canvas.concat(lastMatrix);
                                    canvas.drawBitmap(lastBitmap, null, this.activeImageDestRect, this.activeImagePaint);
                                }
                            } else {
                                canvas.concat(adjustmentMatrix);
                                canvas.drawBitmap(bitmap, null, this.activeImageDestRect, this.activeImagePaint);
                                lastBitmap = bitmap;
                            }
                        }
                    }
                } finally {
                    if (canvas != null) {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
