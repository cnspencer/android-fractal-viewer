package com.charlestheman.basicfractal;

import java.io.Closeable;

/**
 * An iterator for performing the calculations needed for the fractal.  It requires CalcInfo,
 * which represents the data at each iterator position that are needed to perform the calculations,
 * and the Result, which represents the result of the calculation
 * <p/>
 * Note that this interface is different enough from Iterator<T> that it can't be effectively
 * extended without violating LSP.
 */
public interface CalculationIterator<CalcInfo, Result> extends Closeable {
    /**
     * @return Returns true if there are more calculations to do, false otherwise
     */
    boolean hasNext();

    /**
     * The iterator will advance to the next calculation, and the argument will be filled with
     * information about how to perform this calculation
     *
     * @param calcInfo The object that this method will change to hold data about how to perform
     *                 the next calculation
     */
    void next(CalcInfo calcInfo);

    /**
     * Sets the result for the current calculation by copying data from result
     *
     * @param result The object containing the calculation result data to be copied
     */
    void set(Result result);

    /**
     * Close the iterable to indicate that set() will no longer be called
     */
    @Override
    void close();
}
