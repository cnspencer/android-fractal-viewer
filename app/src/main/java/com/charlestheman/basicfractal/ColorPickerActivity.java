package com.charlestheman.basicfractal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;

import java.util.ArrayList;
import java.util.List;

public class ColorPickerActivity extends Activity {
    public enum Params {
        NEW_COLOR("newColor"),
        OLD_COLOR("oldColor");

        private String name;
        public String getName() { return name; }
        Params(String name) { this.name = name; }
    }

    public enum Results {
        COLOR("color");

        private String name;
        public String getName() { return name; }
        Results(String name) { this.name = name; }
    }

    private ColorPicker picker = null;
    private SaturationBar saturationBar = null;
    private ValueBar valueBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_picker);

        // get intent
        boolean newColor = this.getIntent().getBooleanExtra(Params.NEW_COLOR.getName(), true);
        int oldColor = this.getIntent().getIntExtra(Params.OLD_COLOR.getName(), 0);

        picker = (ColorPicker) findViewById(R.id.picker);

        saturationBar = (SaturationBar) findViewById(R.id.saturation_bar);
        picker.addSaturationBar(saturationBar);

        valueBar = (ValueBar) findViewById(R.id.value_bar);
        picker.addValueBar(valueBar);

        // The color must be set up after the saturation and value bars are added.
        // Otherwise, it will show the wrong color.
        if (newColor) {
            picker.setShowOldCenterColor(false);
        } else {
            picker.setShowOldCenterColor(true);
            picker.setOldCenterColor(oldColor);
            picker.setColor(oldColor);
        }

        Button okButton = (Button) findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra(Results.COLOR.getName(), picker.getColor()); // ARGB
                setResult(RESULT_OK, data);
                finish();
            }
        });

        Button cancelButton = (Button) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                setResult(RESULT_CANCELED, data);
                finish();
            }
        });
    }
}
