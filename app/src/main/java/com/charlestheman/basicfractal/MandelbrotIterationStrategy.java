package com.charlestheman.basicfractal;

/**
 * Implementation of IterationStrategy for the Mandelbrot set algorithm
 */
public class MandelbrotIterationStrategy implements IterationStrategy {
    private int maxIterations = 0;

    public MandelbrotIterationStrategy(final int maximumEscapeIterations) {
        this.maxIterations = maximumEscapeIterations;
    }

    @Override
    public int getEscapeIterations(final double x0, final double y0) {
        double x = x0;
        double y = y0;

        int iteration = 0;

        // calculate the number of iterations to escape
        while (x * x + y * y <= 4 && iteration < this.maxIterations) {
            final double xTemp = x * x - y * y + x0; // the real part
            y = 2 * x * y + y0; // the imaginary part

            x = xTemp;
            ++iteration;
        }

        return iteration;
    }

    @Override
    public int getMaximumEscapeIterations() {
        return this.maxIterations;
    }
}
