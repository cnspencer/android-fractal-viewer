package com.charlestheman.basicfractal;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.vecmath.Matrix3d;

/**
 * Handles the rendering of multiple images of increasing resolution
 */
public class RenderingManager {
    private static final double MINIMUM_INTERMEDIATE_WIDTH = 16d;
    private static final double MINIMUM_INTERMEDIATE_HEIGHT = 16d;
    private static final int BACKGROUND_THREADS = Runtime.getRuntime().availableProcessors();

    private final CalculationTaskManager calculationTaskManager = new CalculationTaskManager(BACKGROUND_THREADS);
    private final List<SampleManager> sampleManagers = new ArrayList<>();

    private ColoringStrategy coloringStrategy = null;

    private int testCount = 0;
    private int lastDoneIndex = -1;
    private Bitmap lastDoneBitmap = null;
    private int lastAverageEdgeColor = 0xFF000000;
    private boolean cancelling = false;

    private final double maxWidth;
    private final double maxHeight;

    public RenderingManager(int maxWidth, int maxHeight) {
        if (maxWidth < MINIMUM_INTERMEDIATE_WIDTH || maxHeight < MINIMUM_INTERMEDIATE_HEIGHT)
            throw new IllegalArgumentException("maxWidth and maxHeight must be at least equal to the minimums!");

        this.maxWidth = (double)maxWidth;
        this.maxHeight = (double)maxHeight;

        double heightD = maxHeight;
        double widthD = maxWidth;

        try {
            do {
                SampleManager sm;
                sm = new SampleManager(widthD, heightD);

                sampleManagers.add(sm);

                heightD = heightD / 2d;
                widthD = widthD / 2d;
            }
            while (heightD >= MINIMUM_INTERMEDIATE_HEIGHT && widthD >= MINIMUM_INTERMEDIATE_WIDTH);
        } catch (RuntimeException ex) {
            Log.e(RenderingManager.class.getName(), "Could not create all SampleManagers due to lack of memory!", ex);
        }

        // be sure the smallest one comes first
        Collections.reverse(sampleManagers);
    }

    public double getMaxWidth() {
        return maxWidth;
    }

    public double getMaxHeight() {
        return maxHeight;
    }

    /**
     * Returns true if any calculations are still happening.  This can be true even for a short while
     * after cancel() is called.
     *
     * @return True if calculating, false otherwise
     */
    public boolean isCalculating() {
        for (SampleManager sm : sampleManagers) {
            if (sm.isCalculating()) {
                return true;
            }
        }
        return false;
    }

    private void setAllReady() {
        if (isCalculating()) {
            throw new NullPointerException("setAllReady() attempted while calculations in progress!");
        }
        for (SampleManager sm : sampleManagers) {
            sm.reset();
        }
    }

    public void start() {
        if (isCalculating()) {
            throw new RuntimeException("RenderingManager is not ready for start()!");
        } else if (coloringStrategy == null) {
            throw new NullPointerException("coloringStrategy must be non-null in start()!");
        }
        setAllReady();
        lastDoneIndex = -1;
        testCount = -1;
        lastDoneBitmap = null;
        cancelling = false;

        for (SampleManager sm : sampleManagers) {
            calculationTaskManager.calculateFractal(sm, coloringStrategy);
        }
    }

    public void cancel() {
        lastDoneIndex = -1;
        testCount = -1;
        lastDoneBitmap = null;
        calculationTaskManager.cancel();
        cancelling = true;
    }

    public Bitmap getCurrentBitmap() {
        if (cancelling) {
            return null;
        }
        SampleManager lastDone = null;
        int i = -1;
        for (SampleManager sm : sampleManagers) {
            if (sm.isDone()) {
                lastDone = sm;
                ++i;
            } else {
                break;
            }
        }
        if (lastDone != null && this.lastDoneIndex < i) {
            this.lastDoneIndex = i;
            lastDoneBitmap = Bitmap.createBitmap(lastDone.getSampleBuffer(), lastDone.getSampleWidth(),
                    lastDone.getSampleHeight(), Bitmap.Config.ARGB_4444);
            lastAverageEdgeColor = calculateAverageEdgeColor(lastDone.getSampleBuffer(), lastDone.getSampleWidth(),
                    lastDone.getSampleHeight());
            return lastDoneBitmap;
        }
        return lastDoneBitmap;
    }

//    public Bitmap getCurrentBitmapTESTEST() { /// TODO: testing code!!!
//        if (cancelling) {
//            return null;
//        }
//        SampleManager lastDone = null;
//        int i = -1;
//        for (SampleManager sm : sampleManagers) {
//            if (sm.isDone()) {
//                lastDone = sm;
//                ++i;
//                if (i >= testCount) {
//                    ++testCount;
//                    break;
//                }
//            } else {
//                break;
//            }
//        }
//        if (lastDone != null && this.lastDoneIndex < i) {
//            this.lastDoneIndex = i;
//            lastDoneBitmap = Bitmap.createBitmap(lastDone.getSampleBuffer(), lastDone.getSampleWidth(),
//                    lastDone.getSampleHeight(), Bitmap.Config.ARGB_8888);
//            return lastDoneBitmap;
//        }
//        return lastDoneBitmap;
//    }

    public void setColoringStrategy(ColoringStrategy coloringStrategy) {
        if (this.isCalculating()) {
            throw new RuntimeException("Cannot make change while calculating!");
        }
        this.coloringStrategy = coloringStrategy;
    }

    public void setPositionMatrix3d(final Matrix3d positionMatrix3d) {
        if (this.isCalculating()) {
            throw new RuntimeException("Cannot make change while calculating!");
        }
        for (SampleManager sm : sampleManagers) {
            sm.setPositionMatrix3d(new Matrix3d(positionMatrix3d));
        }
    }

    public int getLastAverageEdgeColor() {
        return this.lastAverageEdgeColor;
    }

    private int calculateAverageEdgeColor(int[] samples, int width, int height) {
        int redSum = 0;
        int blueSum = 0;
        int greenSum = 0;
        int total = 2 * (width + height);

        // Note that corners count double

        // top edge
        for (int i = 0; i < width; ++i) {
            redSum += (samples[i] & 0x00FF0000) >> 16;
            greenSum += (samples[i] & 0x0000FF00) >> 8;
            blueSum += (samples[i] & 0x000000FF);
        }

        // bottom edge
        for (int i = 0; i < width; ++i) {
            redSum += (samples[(height - 1) * width + i] & 0x00FF0000) >> 16;
            greenSum += (samples[(height - 1) * width + i] & 0x0000FF00) >> 8;
            blueSum += (samples[(height - 1) * width + i] & 0x000000FF);
        }

        // left edge
        for (int j = 0; j < height; ++j) {
            redSum += (samples[j * width] & 0x00FF0000) >> 16;
            greenSum += (samples[j * width] & 0x0000FF00) >> 8;
            blueSum += (samples[j * width] & 0x000000FF);
        }

        // right edge
        for (int j = 0; j < height; ++j) {
            redSum += (samples[j * width + (width - 1)] & 0x00FF0000) >> 16;
            greenSum += (samples[j * width + (width - 1)] & 0x0000FF00) >> 8;
            blueSum += (samples[j * width + (width - 1)] & 0x000000FF);
        }

        int redAverage = redSum / total;
        int greenAverage = greenSum / total;
        int blueAverage = blueSum / total;

        return 0xFF000000 + (redAverage << 16) + (greenAverage << 8) + blueAverage;
    }
}
