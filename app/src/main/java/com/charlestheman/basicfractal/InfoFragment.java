package com.charlestheman.basicfractal;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.DialogFragment;

/**
 * This fragment is used in a dialog to show positioning information to the user
 */
public class InfoFragment extends DialogFragment {
    private double x = 0d;
    private double y = 0d;
    private double scale = 0d;
    private double rotation = 0d;

    @Override
    public void setArguments(Bundle bundle) {
        x = bundle.getDouble("x");
        y = bundle.getDouble("y");
        scale = bundle.getDouble("scale");
        rotation = bundle.getDouble("rotation");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String message = "x = " + x + "\ny = " + y + "\n" +
                "scale = " + scale + "\n" +
                "rotation = " + rotation;
        builder.setMessage(message)
                .setTitle(R.string.info_dialog_title)
                .setNeutralButton("OK", null);
        return builder.create();
    }
}
