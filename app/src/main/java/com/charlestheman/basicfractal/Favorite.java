package com.charlestheman.basicfractal;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;

import javax.vecmath.Matrix3d;

/**
 * Represents a position, coloring pattern, and fractal that a user has made a favorite.  An instance of this
 * class may exist even if it is not yet persisted, and this class manages querying existence on the drive,
 * saving a new favorite to the drive, and loading one from the drive.
 * <p/>
 * It must be created with a desired directory name in mind, which generally would be provided from
 * FavoritesManager.
 */
public class Favorite {
    private static final int THUMBNAIL_HEIGHT = 128;
    private static final int THUMBNAIL_WIDTH = 128;
    private static final String DATA_FILE_NAME = "data.json";
    private static final String DATA_FILE_NAME_V1 = "data.xml";
    private static final String THUMBNAIL_FILE_NAME = "thumbnail.png";

    private final File directory;
    private FractalPojo fractalPojo = null;
    private Bitmap thumbnail = null;

    public Favorite(@NonNull File directory) {
        this.directory = directory;
    }

    private Bitmap scaleCenterCrop(@NonNull Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    public Bitmap getThumbnail() {
        if (thumbnail != null) {
            return thumbnail;
        }
        String imgFileName = directory.getAbsolutePath() + File.separator + THUMBNAIL_FILE_NAME;
        File imgFile = new File(imgFileName);

        if (imgFile.exists()) {
            return BitmapFactory.decodeFile(imgFileName);
        }
        return null;
    }

    public void setThumbnailFromSource(@NonNull Bitmap srcBitmap) {
        thumbnail = scaleCenterCrop(srcBitmap, THUMBNAIL_HEIGHT, THUMBNAIL_WIDTH);
    }

    private FractalPojo getFractalPojo_v1() {
        File xmlFile = new File(directory.getAbsolutePath() + File.separator + DATA_FILE_NAME_V1);
        if (!xmlFile.exists()) {
            return null;
        }

        Matrix3d newPositionMatrix = null;
        ColoringStrategy cs = null;
        Reader in = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            in = new BufferedReader(new FileReader(xmlFile));

            xpp.setInput(in);
            int eventType = xpp.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equals("PositionMatrix")) {
                        eventType = xpp.next();
                        if (eventType == XmlPullParser.TEXT) {
                            String matrixStr = xpp.getText();
                            String[] doubleArray = matrixStr.trim().split("\\s*,\\s*");
                            newPositionMatrix = new Matrix3d();
                            newPositionMatrix.m00 = Double.parseDouble(doubleArray[0]);
                            newPositionMatrix.m01 = Double.parseDouble(doubleArray[1]);
                            newPositionMatrix.m02 = Double.parseDouble(doubleArray[2]);
                            newPositionMatrix.m10 = Double.parseDouble(doubleArray[3]);
                            newPositionMatrix.m11 = Double.parseDouble(doubleArray[4]);
                            newPositionMatrix.m12 = Double.parseDouble(doubleArray[5]);
                            newPositionMatrix.m20 = Double.parseDouble(doubleArray[6]);
                            newPositionMatrix.m21 = Double.parseDouble(doubleArray[7]);
                            newPositionMatrix.m22 = Double.parseDouble(doubleArray[8]);
                        } else continue;
                    } else if (xpp.getName().equals("ColorScheme")) {
                        String type = xpp.getAttributeValue("", "type");
                        if (type != null && type.equals("TrigColorScheme.Predefined")) {
                            String predefinedName = xpp.getAttributeValue("", "predefined");
                            if (predefinedName != null) {
                                cs = ColoringStrategies.getByName(predefinedName);
                                if (cs == null) {
                                    cs = ColoringStrategies.getDefault();
                                }
                                // Note: using default here.  Previously it was using what was already in shared preferences, but in
                                // neither case was it using a value from the file TODO?
                                IterationStrategy is = new MandelbrotIterationStrategy(FractalView.DEFAULT_MAX_ITERATIONS);
                                cs.setIterationStrategy(is);
                            }
                        }
                    }
                }
                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (cs != null && newPositionMatrix != null) {
            FractalPojo fractalPojo = new FractalPojo();
            fractalPojo.setColoringStrategy(cs);
            fractalPojo.setPositionMatrix(newPositionMatrix);
            return fractalPojo;
        } else {
            return null;
        }
    }

    /**
     * This will return a FractalPojo.  If this hasn't already been retrieved, it retrieves it from
     * memory.
     *
     * If the version is v1.2 or earlier, then the position matrix will need to be converted
     * from screen space to world space and we will need the currentViewWidth and currentViewHeight.
     * TODO only works for portrait mode. (landscape works on newer versions)
     *
     * @param currentViewWidth View width, needed when converting position matrix from screen to world space
     * @param currentViewHeight View height, needed when converting position matrix from screen to world space
     * @return
     */
    public FractalPojo getFractalPojo(double currentViewWidth, double currentViewHeight) {
        if (fractalPojo != null) {
            return fractalPojo;
        } else {
            if (!directory.exists()) {
                return null;
            }

            File jsonFile = new File(directory.getAbsoluteFile() + "/" + DATA_FILE_NAME);
            if (!jsonFile.exists()) {
                fractalPojo = getFractalPojo_v1();
                // convert from screen space matrix to world space matrix
                convertPositionMatrix(fractalPojo.getPositionMatrix(), currentViewWidth, currentViewHeight);
                return fractalPojo;
            }

            Reader in = null;

            try {
                in = new BufferedReader(new FileReader(jsonFile));
                Gson gson = new Gson();
                fractalPojo = gson.fromJson(in, FractalPojo.class);
                if (fractalPojo.getVersion() < FractalPojo.POSITION_MATRIX_CORRECTION_VERSION) {
                    convertPositionMatrix(fractalPojo.getPositionMatrix(), currentViewWidth, currentViewHeight);
                }
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (fractalPojo.getVersion() < FractalPojo.ROTATION_SCALE_ADDED_VERSION) {
                fractalPojo.setScale(fractalPojo.getPositionMatrix().m00);
            }

            return fractalPojo;
        }
    }

    /**
     * Converts the position matrix from screen space to world/unit/fractal space
     */
    private void convertPositionMatrix(Matrix3d positionMatrix, double currentViewWidth, double currentViewHeight) {
        Matrix3d temp = new Matrix3d(positionMatrix);

        Matrix3d v1_2ProjectionMatrix = setUpV1_2ProjectionMatrix(currentViewWidth, currentViewHeight);
        temp.mul(v1_2ProjectionMatrix, temp);

        // cancel out the new project matrix, using the old project formula instead
        ProjectionMatrix projectionMatrix = new ProjectionMatrix(currentViewWidth, currentViewHeight);
        projectionMatrix.invert();
        temp.mul(temp, projectionMatrix);

        positionMatrix.set(temp);
    }
    private Matrix3d setUpV1_2ProjectionMatrix(double currentViewWidth, double currentViewHeight) {
        // flip the Y dimension
        Matrix3d reflectYMatrix3d =
                new Matrix3d(
                        1d, 0d, 0d,
                        0d, -1d, 0d,
                        0d, 0d, 1d
                );

        // scale such that the smallest dimension, sampleWidth, has a sampleWidth of 2
        Matrix3d scaleMatrix3d =
                new Matrix3d(
                        4d / currentViewWidth, 0d, 0d,
                        0d, 4d / currentViewWidth, 0d,
                        0d, 0d, 1d
                );

        // shift so that the center is at the origin
        Matrix3d translationMatrix3d =
                new Matrix3d(
                        1d, 0d, -2d,
                        0d, 1d, 2d * (currentViewHeight / currentViewWidth),
                        0d, 0d, 1d
                );

        Matrix3d projectionMatrix3d = new Matrix3d();
        projectionMatrix3d.setIdentity();
        projectionMatrix3d.mul(reflectYMatrix3d, projectionMatrix3d);
        projectionMatrix3d.mul(scaleMatrix3d, projectionMatrix3d);
        projectionMatrix3d.mul(translationMatrix3d, projectionMatrix3d);
        return projectionMatrix3d;
    }

    public void setFractalPojo(@NonNull FractalPojo fractalPojo) {
        this.fractalPojo = fractalPojo;
    }

    public boolean save() {
        if (fractalPojo == null) {
            throw new RuntimeException("fractalPojo is null!");
        }

        if (!directory.exists()) {
            boolean result = directory.mkdir();
            if (!result) {
                return false;
            }
        }

        File file = new File(directory.getAbsoluteFile() + "/" + DATA_FILE_NAME);
        PrintWriter pw = null;
        try {
            Gson gson = new Gson();
            String str = gson.toJson(fractalPojo);

            pw = new PrintWriter(file);
            pw.println(str);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (pw != null) {
                pw.close();
            }
        }

        Bitmap thumbnail = this.getThumbnail();

        OutputStream out = null;
        File thumbnailFile = new File(directory.getAbsoluteFile() + "/" + THUMBNAIL_FILE_NAME);
        try {
            out = new BufferedOutputStream(new FileOutputStream(thumbnailFile));
            thumbnail.compress(Bitmap.CompressFormat.PNG, 50, out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public void delete() throws IOException {
        FileUtils.deleteDirectory(directory);
    }

    public boolean exists() {
        return directory.exists();
    }

    public File getDirectory() {
        return this.directory;
    }
}
