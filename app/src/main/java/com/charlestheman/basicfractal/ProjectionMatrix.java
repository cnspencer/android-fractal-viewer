package com.charlestheman.basicfractal;

/**
 * This class is designed to help with common projections between world/fractal space, and image space
 */
public class ProjectionMatrix extends MyMatrix {
    public static final double FRACTAL_SPACE_WIDTH = 2d;
    public static final double FRACTAL_SPACE_HEIGHT = 2d;

    public ProjectionMatrix(double imageWidth, double imageHeight) {
        this.setImageDimensions(imageWidth, imageHeight);
    }

    public void setImageDimensions(double imageWidth, double imageHeight) {
        this.setIdentity();
        this.translate(-imageWidth / 2d, -imageHeight / 2d);
        this.reflectXAxis();
        double smallDimension = imageWidth < imageHeight ? imageWidth : imageHeight;
        this.scale(smallDimension / FRACTAL_SPACE_WIDTH);
    }
}
