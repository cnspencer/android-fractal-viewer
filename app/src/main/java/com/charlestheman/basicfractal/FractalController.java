package com.charlestheman.basicfractal;

import android.support.annotation.NonNull;

import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;

/**
 * Manages changes to positioning, zoom, coloring, iterations, etc.
 */
public class FractalController {
    private static final int DUMMY_WIDTH = 100;
    private static final int DUMMY_HEIGHT = 100;

    private final Object syncObject = new Object();
    private RenderingManager renderingManager = null;
    private boolean changePending = false;
    private boolean cancelRequested = false;
    // maintained data
    private ColoringStrategy coloringStrategy = null;
    private IterationStrategy iterationStrategy = null;
    private final MyMatrix currentPositionMatrix3d = new MyMatrix();
    private final ProjectionMatrix projectionMatrix3d = new ProjectionMatrix(DUMMY_WIDTH, DUMMY_HEIGHT);
    private double totalRotation = 0d;
    private double totalScale = 0d;

    public FractalController() {
        resetPosition();
    }

    public void postChange() {
        synchronized (syncObject) {
            changePending = true;
        }
    }

    public Matrix3d getCurrentPositionMatrix3d() {
        return new Matrix3d(this.currentPositionMatrix3d);
    }


    public void setCurrentPositionMatrix3d(@NonNull Matrix3d matrix) {
        synchronized (syncObject) {
            this.currentPositionMatrix3d.set(matrix);
        }
    }

    public RenderingManager getRenderingManager() {
        return renderingManager;
    }

    public void setRenderingManager(@NonNull RenderingManager renderingManager) {
        synchronized (syncObject) {
            this.renderingManager = renderingManager;
            projectionMatrix3d.setImageDimensions(renderingManager.getMaxWidth(), renderingManager.getMaxHeight());
        }
    }

    public void move(double x, double y) {
        synchronized (syncObject) {
            Vector3d movement = new Vector3d(-x, -y, 0d);
            projectionMatrix3d.transform(movement);
            currentPositionMatrix3d.transform(movement);
            currentPositionMatrix3d.translate(movement.getX(), movement.getY());
        }
    }

    public void scale(double scaleFactor, double focalPointX, double focalPointY) {
        synchronized (syncObject) {

            Vector3d focalPoint = new Vector3d(focalPointX, focalPointY, 1d); // screen space
            this.projectionMatrix3d.transform(focalPoint);
            this.currentPositionMatrix3d.transform(focalPoint);

            currentPositionMatrix3d.scale(scaleFactor);

            Vector3d focalPoint2 = new Vector3d(focalPointX, focalPointY, 1d); // screen space
            this.projectionMatrix3d.transform(focalPoint2);
            this.currentPositionMatrix3d.transform(focalPoint2);

            currentPositionMatrix3d.translate(
                    focalPoint.getX() - focalPoint2.getX(),
                    focalPoint.getY() - focalPoint2.getY()
            );

            totalScale *= 1d / scaleFactor;
        }
    }

    /**
     * Rotates the view clockwise for positive lambda
     * @param angle angle of clockwise rotation
     */
    public void rotate(double angle, double focalPointX, double focalPointY) {

        Vector3d focalPoint = new Vector3d(focalPointX, focalPointY, 1d); // screen space
        this.projectionMatrix3d.transform(focalPoint);
        this.currentPositionMatrix3d.transform(focalPoint);

        currentPositionMatrix3d.rotate(angle);

        Vector3d focalPoint2 = new Vector3d(focalPointX, focalPointY, 1d); // screen space
        this.projectionMatrix3d.transform(focalPoint2);
        this.currentPositionMatrix3d.transform(focalPoint2);

        currentPositionMatrix3d.translate(
                focalPoint.getX() - focalPoint2.getX(),
                focalPoint.getY() - focalPoint2.getY()
        );

        totalRotation += angle;
        if (totalRotation > Math.PI) {
            totalRotation = totalRotation - 2 * Math.PI;
        } else if (totalRotation < -Math.PI) {
            totalRotation = totalRotation + 2 * Math.PI;
        }
    }

    public void resetPosition() {
        synchronized (syncObject) {
            this.currentPositionMatrix3d.setIdentity();
            this.currentPositionMatrix3d.scale(0.5d);
            this.totalRotation = 0d;
            this.totalScale = 2d;
        }
    }

    public void setMaxIterations(int maxIterations) {
        synchronized (syncObject) {
            this.iterationStrategy = new MandelbrotIterationStrategy(maxIterations);
            if (coloringStrategy != null) {
                this.coloringStrategy.setIterationStrategy(this.iterationStrategy);
            }
        }
    }

    public ColoringStrategy getColoringStrategy() {
        return this.coloringStrategy;
    }

    public void setColoringStrategy(ColoringStrategy coloringStrategy) {
        synchronized (syncObject) {
            this.coloringStrategy = coloringStrategy;
            this.coloringStrategy.setIterationStrategy(iterationStrategy);
        }
    }

    public IterationStrategy getIterationStrategy() {
        return this.iterationStrategy;
    }

    public void setIterationStrategy(IterationStrategy is) {
        this.iterationStrategy = is;
        if (this.coloringStrategy != null) {
            this.coloringStrategy.setIterationStrategy(is);
        }
    }

    public double getTotalRotation() {
        return totalRotation;
    }

    public void setTotalRotation(double totalRotation) {
        this.totalRotation = totalRotation;
    }

    public double getTotalScale() {
        return this.totalScale;
    }

    public void setTotalScale(double totalScale) {
        this.totalScale = totalScale;
    }

    public Vector3d getCurrentPosition() {
        Vector3d position;
        if (renderingManager == null) {
            position = new Vector3d(DUMMY_WIDTH / 2d, DUMMY_HEIGHT / 2d, 1d);
        } else {
            position = new Vector3d(renderingManager.getMaxWidth() / 2d, renderingManager.getMaxHeight() / 2d, 1d);
        }
        this.projectionMatrix3d.transform(position);
        this.currentPositionMatrix3d.transform(position);
        return position;
    }

    public void tick() {
        if (changePending && renderingManager != null) {
            synchronized (syncObject) {
                if (renderingManager.isCalculating()) {
                    if (!cancelRequested) {
                        renderingManager.cancel();
                        cancelRequested = true;
                    }
                } else {
                    renderingManager.setPositionMatrix3d(currentPositionMatrix3d);
                    renderingManager.setColoringStrategy(coloringStrategy);
                    renderingManager.start();
                    changePending = false;
                    cancelRequested = false;
                }
            }
        }
    }

}
