package com.charlestheman.basicfractal;

import java.io.Serializable;

/**
 * Interface for a strategy for defining colors for (x,y) coordinates, assuming the use of an IterationStrategy
 * This class is NOT immutable. (would be nice, though)
 */
public abstract class ColoringStrategy implements Serializable {
    private IterationStrategy iterationStrategy = null;

    protected ColoringStrategy(ColoringStrategy coloringStrategy) {
        if (coloringStrategy != null) {
            setIterationStrategy(coloringStrategy.getIterationStrategy());
        }
    }

    /**
     * @return The IterationStrategy for a fractal that can be computed in this way
     */
    public IterationStrategy getIterationStrategy() {
        return this.iterationStrategy;
    }

    /**
     * Sets the IterationStrategy for the fractal
     *
     * @param iterationStrategy the IterationStrategy to calculate the fractal
     */
    public void setIterationStrategy(IterationStrategy iterationStrategy) {
        this.iterationStrategy = iterationStrategy;
    }

    /**
     * Method to return the color in ARGB format as an int (for the sake of efficiency)
     *
     * @param x X coordinate
     * @param y Y coordinate
     * @param hsvBuffer HSV buffer to be provided by caller so that this class can remain thread-safe
     * @return The color as an int in A8R8G8B8 format
     */
    public abstract int getColor(final double x, final double y, final float[] hsvBuffer);
}
