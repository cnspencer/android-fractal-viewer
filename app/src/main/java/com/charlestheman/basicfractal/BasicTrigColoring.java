package com.charlestheman.basicfractal;

/**
 * Implementation of ColoringStrategy for basic sinusoidal coloring
 */
public class BasicTrigColoring extends ColoringStrategy {
    private static final double REFINEMENT_DIVISOR = 4.0d;

    private double redOffsetRadians = 0.0d;
    private double greenOffsetRadians = 0.0d;
    private double blueOffsetRadians = 0.0d;

    private double redFrequency = 1.0d;
    private double greenFrequency = 1.0d;
    private double blueFrequency = 1.0d;

    public BasicTrigColoring(double redOffsetRadians, double greenOffsetRadians, double blueOffsetRadians) {
        this(redOffsetRadians, greenOffsetRadians, blueOffsetRadians, 1.0d, 1.0d, 1.0d);
    }

    public BasicTrigColoring(double redOffsetRadians, double greenOffsetRadians, double blueOffsetRadians,
                             double redFrequency, double greenFrequency, double blueFrequency) {
        super(null);
        this.redOffsetRadians = redOffsetRadians;
        this.greenOffsetRadians = greenOffsetRadians;
        this.blueOffsetRadians = blueOffsetRadians;

        this.redFrequency = redFrequency;
        this.greenFrequency = greenFrequency;
        this.blueFrequency = blueFrequency;
    }

    @Override
    public int getColor(final double x, final double y, final float[] hsvBuffer) {
        int iterations = this.getIterationStrategy().getEscapeIterations(x, y);
        if (iterations >= this.getIterationStrategy().getMaximumEscapeIterations()) {
            return 0xFF000000; // black
        }

        double refinedIterations = Math.sqrt(iterations) / REFINEMENT_DIVISOR;

        // red part
        int color = (int) ((Math.sin(redFrequency * refinedIterations + this.redOffsetRadians) + 1.0d) * 127d) << 16;
        // green
        color |= (int) ((Math.sin(greenFrequency * refinedIterations + this.greenOffsetRadians) + 1.0d) * 127d) << 8;
        // blue
        color |= (int) ((Math.sin(blueFrequency * refinedIterations + this.blueOffsetRadians) + 1.0d) * 127d);
        return color | 0xFF000000;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (!(obj instanceof BasicTrigColoring)) {
            return false;
        } else {
            BasicTrigColoring btc = (BasicTrigColoring) obj;
            return this.redOffsetRadians == btc.redOffsetRadians &&
                    this.greenOffsetRadians == btc.greenOffsetRadians &&
                    this.blueOffsetRadians == btc.blueOffsetRadians &&
                    this.redFrequency == btc.redFrequency &&
                    this.greenFrequency == btc.greenFrequency &&
                    this.blueFrequency == btc.blueFrequency;
        }
    }

    public double getRedOffsetRadians() {
        return redOffsetRadians;
    }

    public double getGreenOffsetRadians() {
        return greenOffsetRadians;
    }

    public double getBlueOffsetRadians() {
        return blueOffsetRadians;
    }

    public double getRedFrequency() {
        return redFrequency;
    }

    public double getGreenFrequency() {
        return greenFrequency;
    }

    public double getBlueFrequency() {
        return blueFrequency;
    }
}
