package com.charlestheman.basicfractal;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadFactory;

/**
 * Manages the thread pool for fractal calculations
 */
public class CalculationTaskManager {
    private final Object syncObject = new Object();
    private ExecutorService executorService = null;
    private List<CalculationTask> tasks = new ArrayList<>();

    public CalculationTaskManager(int threadCount) {
        executorService = Executors.newFixedThreadPool(threadCount, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setPriority(Thread.MIN_PRIORITY);
                return thread;
            }
        });
    }

    public void calculateFractal(SampleManager samples, ColoringStrategy coloringStrategy) {
        synchronized (syncObject) {
            for (CalculationIterator<SampleManager.CalcInfo, SampleManager.Result> calcIterator :
                    samples.iterators(samples.getSampleHeight())) {
                CalculationTask task = new CalculationTask(calcIterator, coloringStrategy);
                tasks.add(task);
                FutureTask<Void> futureTask = new CloseableFutureTask(task, calcIterator);
                executorService.submit(futureTask);
            }
        }
    }

    public void cancel() {
        synchronized (syncObject) {
            for (CalculationTask task : this.tasks) {
                task.cancel();
            }
            this.tasks = new ArrayList<>();
        }
    }

    private void removeTask(CalculationTask calculationTask) {
        synchronized (syncObject) {
            tasks.remove(calculationTask);
        }
    }

    private class CloseableFutureTask extends FutureTask<Void> {
        private Closeable closeable = null;
        private CalculationTask calculationTask = null;

        public CloseableFutureTask(CalculationTask calcTask, Closeable closeable) {
            super(calcTask);
            this.calculationTask = calcTask;
            this.closeable = closeable;
        }

        @Override
        public void done() {
            try {
                removeTask(this.calculationTask);
                this.closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
