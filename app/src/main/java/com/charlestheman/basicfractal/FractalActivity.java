package com.charlestheman.basicfractal;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.app.DialogFragment;

import java.io.File;

import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;

public class FractalActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int REQUEST_FAVORITE = 1;
    private static final int REQUEST_COLOR_SCHEME = 2;

    public static final String INFO_DIALOG = "info_dialog";

    private static Context context = null;
    private FractalView fractalView = null;
    private boolean mustRedraw = false;
    private FavoritesManager favoritesManager = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get handles to the FractalView from XML
        this.fractalView = (FractalView) findViewById(R.id.fractal);

        if (!mustRedraw && savedInstanceState != null) {
            fractalView.restoreFromBundle(savedInstanceState);
        }

        mustRedraw = false;

        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        context = this.getBaseContext();

        favoritesManager = new FavoritesManager(context);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        fractalView.storeToBundle(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        boolean allowRotation = sharedPref.getBoolean(SettingsFragment.PREF_ALLOW_ROTATION, true);
        fractalView.setAllowRotation(allowRotation);

        if (mustRedraw) {
            mustRedraw = false;

            String maxIterationsPref = sharedPref.getString(SettingsFragment.PREF_MAX_ITERATIONS,
                    Integer.valueOf(FractalView.DEFAULT_MAX_ITERATIONS).toString());
            int maxIteration = Integer.valueOf(maxIterationsPref);

            fractalView.setMaxIterations(maxIteration);

            String coloringName = sharedPref.getString(SettingsFragment.PREF_COLOR_SCHEME, "Ocean");
            ColoringStrategy cs = ColoringStrategies.getByName(coloringName);
            if (cs == null) {
                ColorScheme colorScheme = new ColorSchemeManager(getBaseContext()).getCurrentColorScheme();
                cs = colorScheme;
            }
            fractalView.setColoringStrategy(cs);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_favorite: {
                onMenuFavoriteSelected();
                return true;
            }

            case R.id.menu_view_favorites: {
                Intent i = new Intent(FractalActivity.this, FavoritesActivity.class);
                this.startActivityForResult(i, REQUEST_FAVORITE);
                return true;
            }

            case R.id.menu_restart: {
                this.fractalView.restart();
                return true;
            }

            case R.id.menu_save_image: {
                onMenuSaveImage();
                return true;
            }

            case R.id.menu_settings: {
                Intent i = new Intent(FractalActivity.this, SettingsActivity.class);
                startActivity(i);
                return true;
            }

            case R.id.menu_info: {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag(INFO_DIALOG);
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                double scale = fractalView.getTotalScale();
                Vector3d posVec = fractalView.getCurrentPosition();
                double rotation = -fractalView.getTotalRotation();

                DialogFragment dialog = new InfoFragment();
                Bundle args = new Bundle();
                args.putDouble("x", posVec.x);
                args.putDouble("y", posVec.y);
                args.putDouble("scale", scale);
                args.putDouble("rotation", rotation);
                dialog.setArguments(args);

                dialog.show(ft, INFO_DIALOG);
                return true;
            }

            case R.id.menu_color_schemes: {
                Intent i = new Intent(FractalActivity.this, ColorSchemeListActivity.class);
                startActivityForResult(i, REQUEST_COLOR_SCHEME);
                return true;
            }

            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void onMenuSaveImage() {
        Toast toast = Toast.makeText(this.getApplicationContext(), "Saving image...", Toast.LENGTH_LONG);
        toast.show();

        Bitmap bitmap = this.fractalView.getBitmap();
        if (bitmap != null) {
            AsyncTask<Bitmap, Void, Boolean> task = new AsyncTask<Bitmap, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Bitmap... params) {
                    String str = MediaStore.Images.Media.insertImage(getContentResolver(), params[0], "Fractal", "Fractal");

                    return str != null;
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    if (result) {
                        Toast.makeText(FractalActivity.this.getApplicationContext(), "Save complete.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FractalActivity.this.getApplicationContext(), "Save failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            };

            task.execute(bitmap);
        }
    }

    private void onMenuFavoriteSelected() {
        Toast toast = Toast.makeText(this.getApplicationContext(), "Saving favorite...", Toast.LENGTH_LONG);
        toast.show();

        class LocalArguments {
            public final Bitmap sourceForThumbnail = FractalActivity.this.fractalView.getBitmap();
            public final FractalPojo fractalPojo = FractalActivity.this.fractalView.getFractalPojo();
            public final FavoritesManager favoritesManager = FractalActivity.this.favoritesManager;
        }

        AsyncTask<LocalArguments, Void, Boolean> task = new AsyncTask<LocalArguments, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(LocalArguments... params) {
                FavoritesManager favoritesManager = params[0].favoritesManager;

                Favorite favorite = favoritesManager.createFavorite();
                if (favorite == null) {
                    return false;
                } else {
                    favorite.setThumbnailFromSource(params[0].sourceForThumbnail);
                    favorite.setFractalPojo(params[0].fractalPojo);
                    return favorite.save();
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    Toast.makeText(FractalActivity.this.getApplicationContext(), "Save complete.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(FractalActivity.this.getApplicationContext(), "Save failed!", Toast.LENGTH_SHORT).show();
                }
            }
        };

        task.execute(new LocalArguments());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FAVORITE && data != null) {
            String favoriteDirectory = data.getStringExtra("favoriteDirectory");
            Favorite favorite = new Favorite(new File(favoriteDirectory));

            if (favorite.exists()) {
                fractalView.loadFavorite(favorite);

//                FractalPojo pojo = favorite.getFractalPojo(fractalView.getWidth(), fractalView.getHeight());
//                fractalView.setFractalPojo(pojo);
//
//                if (pojo.getColoringStrategy() instanceof ColorScheme) { // TODO instanceof
//                    ColorScheme colorScheme = (ColorScheme) pojo.getColoringStrategy();
//                    new ColorSchemeManager(getBaseContext()).saveColorSchemeAsCurrent(colorScheme);
//                }
//
//                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
//                SharedPreferences.Editor editor = sharedPref.edit();
//                String basicTrigColoringName = ColoringStrategies.getNameFromColoringStrategy(pojo.getColoringStrategy());
//                editor.putString(SettingsFragment.PREF_COLOR_SCHEME, basicTrigColoringName);
//                editor.putString(SettingsFragment.PREF_MAX_ITERATIONS,
//                        Integer.toString(pojo.getColoringStrategy().getIterationStrategy().getMaximumEscapeIterations()));
//                editor.apply();

                // necessary in case a bundle is restored at the same time we return to this activity from the favorites activity
                mustRedraw = true;
            } else {
                Log.d("Error", "Favorite could not be set from intent returned by FavoriteActivity");
            }
        } else if (requestCode == REQUEST_COLOR_SCHEME && data != null) {
            if (resultCode == RESULT_OK) {
                ColorScheme scheme = null;

                String colorSchemePath = data.getStringExtra(ColorSchemeListActivity.Results.COLOR_SCHEME_PATH.getName());
                if (colorSchemePath != null) {
                    scheme = new ColorScheme(new File(colorSchemePath));
                    scheme.load();
                } else {
                    String colorSchemeName = data.getStringExtra(ColorSchemeListActivity.Results.COLOR_SCHEME_NAME.getName());
                    if (colorSchemeName != null) {
                        scheme = ColorSchemeListActivity.DefaultColorSchemes.getByName(colorSchemeName).getColorScheme();
                    }
                }

                if (scheme != null) {
                    new ColorSchemeManager(getBaseContext()).saveColorSchemeAsCurrent(scheme);

                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(SettingsFragment.PREF_COLOR_SCHEME, ColoringStrategies.CUSTOM.getName());
                    editor.apply();
                    mustRedraw = true;
                }
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsFragment.PREF_MAX_ITERATIONS) ||
                key.equals(SettingsFragment.PREF_COLOR_SCHEME)) {
            mustRedraw = true;
        }
    }
}
