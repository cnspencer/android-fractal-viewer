package com.charlestheman.basicfractal;

import android.view.MotionEvent;

import javax.vecmath.Vector3d;

/**
 * Gesture detector used to handle rotations in response to touch events
 */
public class RotationGestureDetector {
    private static final int FIRST_POINTER_INDEX = 0;
    private static final int SECOND_POINTER_INDEX = 1;

    private Vector3d v1 = new Vector3d();
    private Vector3d v2 = new Vector3d();

    private double totalLambda = 0d;
    private double lambda = 0d;

    private OnRotateGestureListener listener;

    private double focusX = 0d;
    private double focusY = 0d;

    public RotationGestureDetector(OnRotateGestureListener listener) {
        this.listener = listener;
    }

    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                if (event.getPointerCount() == 2){
                    v1.set(event.getX(SECOND_POINTER_INDEX) - event.getX(FIRST_POINTER_INDEX),
                            event.getY(SECOND_POINTER_INDEX) - event.getY(FIRST_POINTER_INDEX),
                            0d);
                    focusX = (event.getX(FIRST_POINTER_INDEX) + event.getX(1)) / 2d;
                    focusY = (event.getY(FIRST_POINTER_INDEX) + event.getY(1)) / 2d;
                }
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                if (v1 != null && event.getPointerCount() >= 2) {
                    try {
                        v2.set(event.getX(SECOND_POINTER_INDEX) -
                                        event.getX(FIRST_POINTER_INDEX),
                                event.getY(SECOND_POINTER_INDEX) -
                                        event.getY(FIRST_POINTER_INDEX),
                                0d);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    // gets the absolute value of the angle between teh vectors
                    double cosine = v1.dot(v2) / (v1.length() * v2.length());
                    cosine = Math.max(-1d, cosine);
                    cosine = Math.min(1d, cosine);
                    double newLambda = Math.acos(cosine);

                    // used to determine if clockwise or counter-clockwise
                    Vector3d cross = new Vector3d();
                    cross.cross(v1, v2);

                    // positive is clockwise
                    if (cross.getZ() > 0) {
                        newLambda = -newLambda;
                    }

                    if (totalLambda != newLambda) {
                        lambda = newLambda - totalLambda;
                        totalLambda = newLambda;
                        listener.onRotate(this);
                    }
                }
                break;
            }

            case MotionEvent.ACTION_UP: {
                reset();
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                reset();
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {
                if (event.getPointerCount() == 0) {
                    reset();
                }
                break;
            }
        }
        return true;
    }

    private void reset() {
        totalLambda = 0d;
    }

    public double getAngle() { return lambda; }

    public double getFocusX() { return focusX; }
    public double getFocusY() { return focusY; }

    public interface OnRotateGestureListener {
        boolean onRotate(RotationGestureDetector detector) ;
    }
}
