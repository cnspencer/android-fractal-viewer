package com.charlestheman.basicfractal;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;


public class FractalView extends SurfaceView implements SurfaceHolder.Callback {
    public static final int DEFAULT_MAX_ITERATIONS = 2000;
    private static final int INVALID_POINTER_ID = -1;
    private static final int DEADZONE_PIXELS = 5;
    private static final boolean DEFAULT_ALLOW_ROTATION = true;

    private Matrix3d restoreMatrix = null;
    private double restoreScale = 2d;
    private double restoreRotation = 0d;
    private RenderThread renderThread = null;
    private ScaleGestureDetector scaleDetector = null;
    private RotationGestureDetector rotationDetector = null;
    private float lastTouchX;
    private float lastTouchY;
    private int activePointerId = INVALID_POINTER_ID;
    private boolean deadZoneLock = true;
    private RenderingManager renderingManager = null;
    private FractalController fractalController = null;
    private int maxIterations = DEFAULT_MAX_ITERATIONS;
    private boolean layoutStarted = false;

    private boolean allowRotation = DEFAULT_ALLOW_ROTATION;

    private Favorite favoriteToLoad = null;

    public FractalView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.getHolder().addCallback(this);
        scaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        rotationDetector = new RotationGestureDetector(new RotationListener());
        this.setFocusable(true);
        this.fractalController = new FractalController();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        layoutStarted = true;

        if (this.renderingManager == null) {
            this.renderingManager = new RenderingManager(this.getWidth(), this.getHeight());
            this.fractalController.setRenderingManager(renderingManager);

            if (favoriteToLoad == null) {
                // iteration count from prefs
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getContext());
                int maxIterations = DEFAULT_MAX_ITERATIONS;
                try {
                    maxIterations = Integer.parseInt(
                            prefs.getString(SettingsFragment.PREF_MAX_ITERATIONS,
                                    String.valueOf(DEFAULT_MAX_ITERATIONS)));
                    this.allowRotation = prefs.getBoolean(SettingsFragment.PREF_ALLOW_ROTATION, DEFAULT_ALLOW_ROTATION);

                    ColoringStrategy cs = ColoringStrategies.getByName(
                            prefs.getString(SettingsFragment.PREF_COLOR_SCHEME, "Ocean"));
                    if (cs == null) {
                        ColorScheme colorScheme = new ColorSchemeManager(getContext()).getCurrentColorScheme();
                        if (colorScheme == null) {
                            cs = ColoringStrategies.getDefault();
                        } else {
                            cs = colorScheme;
                        }
                    }
                    this.fractalController.setColoringStrategy(cs);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    // proceed with default
                }
                this.fractalController.setMaxIterations(maxIterations);
                if (this.restoreMatrix != null) {
                    this.fractalController.setCurrentPositionMatrix3d(this.restoreMatrix);
                    this.fractalController.setTotalRotation(restoreRotation);
                    this.fractalController.setTotalScale(restoreScale);
                }
            } else {
//                setFractalPojo(favoriteToLoad.getFractalPojo(getWidth(), getHeight()));
                loadFavorite(favoriteToLoad);
            }
            clearRestore();
            this.fractalController.postChange();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        this.renderThread = new RenderThread(this.getHolder(), this, renderingManager, fractalController);
        this.renderThread.setRunning(true);
        this.renderThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        renderThread.setRunning(false);

        while (retry) {
            try {
                renderThread.join();
                retry = false;
            } catch (InterruptedException ex) {
                // Try again and again and again
            }
        }

        renderThread = null;
    }

    public int getMaxIterations() {
        return this.maxIterations;
    }

    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
        this.fractalController.setMaxIterations(maxIterations);
        this.fractalController.postChange();
    }

    public void setAllowRotation(boolean allowRotation) {
        this.allowRotation = allowRotation;
    }

    public FractalPojo getFractalPojo() {
        FractalPojo fractalPojo = new FractalPojo();
        fractalPojo.setColoringStrategy(this.fractalController.getColoringStrategy());
        fractalPojo.setPositionMatrix(this.fractalController.getCurrentPositionMatrix3d());
        fractalPojo.setRotationRadians(fractalController.getTotalRotation());
        fractalPojo.setScale(fractalController.getTotalScale());
        return fractalPojo;
    }

    public void setFractalPojo(FractalPojo fractalPojo) {
        this.fractalController.setColoringStrategy(fractalPojo.getColoringStrategy());
        this.fractalController.setIterationStrategy(fractalPojo.getColoringStrategy().getIterationStrategy()); // TODO seems clumsy
        this.fractalController.setCurrentPositionMatrix3d(fractalPojo.getPositionMatrix());
        this.fractalController.setTotalRotation(fractalPojo.getRotationRadians());
        this.fractalController.setTotalScale(fractalPojo.getScale());
        // TODO fractalController's position matrix will be stale
        this.fractalController.postChange();
        clearRestore();
    }

    public void loadFavorite(Favorite favorite) {
        if (layoutStarted) {
            FractalPojo pojo = favorite.getFractalPojo(getWidth(), getHeight());
            setFractalPojo(pojo);

            if (pojo.getColoringStrategy() instanceof ColorScheme) { // TODO instanceof
                ColorScheme colorScheme = (ColorScheme) pojo.getColoringStrategy();
                new ColorSchemeManager(getContext()).saveColorSchemeAsCurrent(colorScheme);
            }

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            String basicTrigColoringName = ColoringStrategies.getNameFromColoringStrategy(pojo.getColoringStrategy());
            editor.putString(SettingsFragment.PREF_COLOR_SCHEME, basicTrigColoringName);
            editor.putString(SettingsFragment.PREF_MAX_ITERATIONS,
                    Integer.toString(pojo.getColoringStrategy().getIterationStrategy().getMaximumEscapeIterations()));
            editor.apply();

            this.favoriteToLoad = null;
        } else {
            this.favoriteToLoad = favorite;
        }
    }

    public void setColoringStrategy(ColoringStrategy strategy) {
        strategy.setIterationStrategy(fractalController.getIterationStrategy());
        fractalController.setColoringStrategy(strategy);
        fractalController.postChange();
    }

    public Matrix3d getCurrentPositionMatrix() { return this.fractalController.getCurrentPositionMatrix3d(); }

    public Vector3d getCurrentPosition() { return this.fractalController.getCurrentPosition(); }

    public double getTotalRotation() { return this.fractalController.getTotalRotation(); }

    public double getTotalScale() { return this.fractalController.getTotalScale(); }

    public void storeToBundle(Bundle bundle) {
        if (fractalController != null) {
            Matrix3d matrix = this.fractalController.getCurrentPositionMatrix3d();
            bundle.putDouble("m00", matrix.m00);
            bundle.putDouble("m01", matrix.m01);
            bundle.putDouble("m02", matrix.m02);
            bundle.putDouble("m10", matrix.m10);
            bundle.putDouble("m11", matrix.m11);
            bundle.putDouble("m12", matrix.m12);
            bundle.putDouble("m20", matrix.m20);
            bundle.putDouble("m21", matrix.m21);
            bundle.putDouble("m22", matrix.m22);
            bundle.putDouble("scale", fractalController.getTotalScale());
            bundle.putDouble("rotation", fractalController.getTotalRotation());
        }
    }

    public void restoreFromBundle(Bundle bundle) {
        // TODO fractalController's position matrix will be stale
        if (restoreMatrix == null) {
            restoreMatrix = new Matrix3d();
        }
        restoreMatrix.m00 = bundle.getDouble("m00");
        restoreMatrix.m01 = bundle.getDouble("m01");
        restoreMatrix.m02 = bundle.getDouble("m02");
        restoreMatrix.m10 = bundle.getDouble("m10");
        restoreMatrix.m11 = bundle.getDouble("m11");
        restoreMatrix.m12 = bundle.getDouble("m12");
        restoreMatrix.m20 = bundle.getDouble("m20");
        restoreMatrix.m21 = bundle.getDouble("m21");
        restoreMatrix.m22 = bundle.getDouble("m22");
        restoreRotation = bundle.getDouble("rotation");
        restoreScale = bundle.getDouble("scale");
    }

    private void clearRestore() {
        restoreMatrix = null;
    }

    public Bitmap getBitmap() {
        return this.renderThread.getLastBitmap();
    }

    public void restart() {
        this.renderThread.restart();
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev) {
        scaleDetector.onTouchEvent(ev);
        if (allowRotation) {
            rotationDetector.onTouchEvent(ev);
        }

        if (renderThread == null || renderingManager == null) {
            return true;
        }

        final int action = ev.getAction();

        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();

                lastTouchX = x;
                lastTouchY = y;
                activePointerId = ev.getPointerId(0);
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                final int pointerIndex = ev.findPointerIndex(activePointerId);

                if (pointerIndex < 0) {
                    break;
                }

                final float x = ev.getX(pointerIndex);
                final float y = ev.getY(pointerIndex);

                // check for deadzone
                if (deadZoneLock && (Math.abs(x - lastTouchX) < DEADZONE_PIXELS || Math.abs(y - lastTouchY) < DEADZONE_PIXELS)) {
                    break;
                }

                deadZoneLock = false;

                if (!scaleDetector.isInProgress()) {
                    // adjust matrix for the image used in the Android Canvas
                    Matrix tempMatrix = renderThread.getAdjustmentMatrix();
                    tempMatrix.postTranslate(x - lastTouchX, y - lastTouchY);

                    fractalController.move((double) x - (double) lastTouchX, (double) y - (double) lastTouchY);
                }

                lastTouchX = x;
                lastTouchY = y;
                break;
            }

            case MotionEvent.ACTION_UP: {
                if (!deadZoneLock) {
                    renderThread.recalculate();
                }
                activePointerId = INVALID_POINTER_ID;
                deadZoneLock = true;
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                activePointerId = INVALID_POINTER_ID;
                deadZoneLock = true;
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {
                final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = ev.getPointerId(pointerIndex);

                if (pointerId == activePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    lastTouchX = ev.getX(newPointerIndex);
                    lastTouchY = ev.getY(newPointerIndex);
                    activePointerId = ev.getPointerId(newPointerIndex);
                }
                break;
            }
        }
        return true;
    }

    private class ScaleListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = detector.getScaleFactor();

            deadZoneLock = false;

            // adjust matrix for the image used in the Android Canvas
            Matrix tempMatrix = renderThread.getAdjustmentMatrix();
            tempMatrix.postScale(scaleFactor, scaleFactor, detector.getFocusX(), detector.getFocusY());

            fractalController.scale(scaleFactor, (double) detector.getFocusX(), (double) detector.getFocusY());

            return true;
        }
    }

    private class RotationListener implements RotationGestureDetector.OnRotateGestureListener {
        @Override
        public boolean onRotate(RotationGestureDetector detector) {
            Matrix tempMatrix = renderThread.getAdjustmentMatrix();
            double radians = detector.getAngle();
            double degrees = (radians * 180) / Math.PI;
            tempMatrix.postRotate(-(float) degrees, (float) detector.getFocusX(), (float) detector.getFocusY());

            fractalController.rotate(-radians, detector.getFocusX(), detector.getFocusY());
            return false;
        }
    }
}
