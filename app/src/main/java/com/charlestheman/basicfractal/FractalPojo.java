package com.charlestheman.basicfractal;

import android.graphics.Color;
import android.support.annotation.NonNull;

import java.io.Serializable;

import javax.vecmath.Matrix3d;

/**
 * Holds all the data to be serialized when saving a position as a favorite
 */
public class FractalPojo implements Serializable {//}, Parcelable {
    public static final int CURRENT_VERSION = 5;
    public static final int POSITION_MATRIX_CORRECTION_VERSION = 3;
    public static final int ROTATION_SCALE_ADDED_VERSION = 4;
    public static final int COLOR_SCHEMES_ADDED_VERSION = 5;

    private int version = CURRENT_VERSION;
    private Matrix3d positionMatrix = null;
    private BasicTrigColoringPojo basicTrigColoringPojo = null;
    private ColorSchemePojo colorSchemePojo = null;
    private MandelbrotIterationStrategyPojo mandelbrotIterationStrategyPojo = null;
    private double rotationRadians = 0d;
    private double scale = 2d;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Matrix3d getPositionMatrix() {
        return positionMatrix;
    }

    public void setPositionMatrix(@NonNull Matrix3d positionMatrix) {
        this.positionMatrix = positionMatrix;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public double getRotationRadians() {
        return rotationRadians;
    }

    public void setRotationRadians(double rotationRadians) {
        this.rotationRadians = rotationRadians;
    }

    public ColoringStrategy getColoringStrategy() {
        ColoringStrategy cs;
        if (basicTrigColoringPojo == null) {
            if (colorSchemePojo == null) {
                return null;
            } else {
                ColorScheme colorScheme = new ColorScheme();
                colorScheme.setFromPojo(colorSchemePojo);
                colorScheme.warm();
                cs = colorScheme;
            }
        } else {
            cs = new BasicTrigColoring(
                    basicTrigColoringPojo.redOffsetRadians,
                    basicTrigColoringPojo.greenOffsetRadians,
                    basicTrigColoringPojo.blueOffsetRadians,
                    basicTrigColoringPojo.redFrequency,
                    basicTrigColoringPojo.greenFrequency,
                    basicTrigColoringPojo.blueFrequency
            );
        }

        if (mandelbrotIterationStrategyPojo != null) {
            IterationStrategy is;
            is = new MandelbrotIterationStrategy(mandelbrotIterationStrategyPojo.maxIterations);
            cs.setIterationStrategy(is);
        }
        return cs;
    }

    public void setColoringStrategy(@NonNull ColoringStrategy coloringStrategy) {
        if (!(coloringStrategy.getIterationStrategy() instanceof MandelbrotIterationStrategy)) {
            throw new RuntimeException("IterationStrategy is null or you made a new subclass without telling FractalPojo!");
        }
        MandelbrotIterationStrategy mis = (MandelbrotIterationStrategy) coloringStrategy.getIterationStrategy();
        MandelbrotIterationStrategyPojo iterationPojo = new MandelbrotIterationStrategyPojo();
        iterationPojo.maxIterations = mis.getMaximumEscapeIterations();
        mandelbrotIterationStrategyPojo = iterationPojo;

        if (coloringStrategy instanceof BasicTrigColoring) {
            BasicTrigColoring btc = (BasicTrigColoring) coloringStrategy;
            BasicTrigColoringPojo coloringPojo = new BasicTrigColoringPojo();
            coloringPojo.redOffsetRadians = btc.getRedOffsetRadians();
            coloringPojo.greenOffsetRadians = btc.getGreenOffsetRadians();
            coloringPojo.blueOffsetRadians = btc.getBlueOffsetRadians();
            coloringPojo.redFrequency = btc.getRedFrequency();
            coloringPojo.greenFrequency = btc.getGreenFrequency();
            coloringPojo.blueFrequency = btc.getBlueFrequency();
            basicTrigColoringPojo = coloringPojo;
            colorSchemePojo = null;
        }
        else if (coloringStrategy instanceof ColorScheme) {
            ColorScheme colorScheme = (ColorScheme) coloringStrategy;
            colorSchemePojo = colorScheme.makeColorSchemePojo();
            basicTrigColoringPojo = null;
        }
        else {
            throw new RuntimeException("FractalPojo not aware of new ColoringStrategy!");
        }
    }

    /**
     * A plain old Java object (pojo) for BasicTrigColoring data
     * Do not change or remove the variables (but feel free to add new ones)
     * <p/>
     * Consider using something like visitor pattern if these become too complicated
     */
    public static class BasicTrigColoringPojo implements Serializable {
        public double redOffsetRadians = 0.0d;
        public double greenOffsetRadians = 0.0d;
        public double blueOffsetRadians = 0.0d;
        public double redFrequency = 1.0d;
        public double greenFrequency = 1.0d;
        public double blueFrequency = 1.0d;

        public BasicTrigColoringPojo() {
        }
    }

    /**
     * A plain old Java object (pojo) for MandelbrotIterationStrategyPojo data
     * Do not change or remove the variables (but feel free to add new ones)
     * <p/>
     * Consider using something like visitor pattern if these become too complicated
     */
    public static class MandelbrotIterationStrategyPojo implements Serializable {
        public int maxIterations = 0;

        public MandelbrotIterationStrategyPojo() {
        }
    }

    //////// for Parcelable ////////////////////

//    private FractalPojo(Parcel in) {
////        this.someBlah = in.readString();
////        moreBlah = new ArrayList<>();
////        in.readTypedList(moreBlah, Blah.CREATOR);
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
////        dest.writeString(this.someBlah);
////        dest.writeTypedList(moreBlah);
//    }
//
//    public static final Parcelable.Creator<FractalPojo> CREATOR = new Parcelable.Creator<FractalPojo>() {
//        public FractalPojo createFromParcel(Parcel source) {
//            return new FractalPojo(source);
//        }
//
//        public FractalPojo[] newArray(int size) {
//            return new FractalPojo[size];
//        }
//    };
}

