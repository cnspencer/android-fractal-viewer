package com.charlestheman.basicfractal;

import android.graphics.Color;

import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * This class allows the storage, retrieval, and interpolation of colors in the HSV color space
 * This class is not thread safe, so it should be copied for use in multiple threads
 */
public class ColorScheme extends ColoringStrategy {
    public static final String COLOR_SCHEMES_NAME_PREFIX = "color_scheme_";
    public static final int MAX_COLORS = 32;

    public static final int HUE_INDEX = 0;
    public static final int SATURATION_INDEX = 1;
    public static final int VALUE_INDEX = 2;

    private static final String DATA_FILE_NAME = "data.json";
    private static final double REFINEMENT_DIVISOR = 12.0d;

    private ArrayList<float[]> hsvComponents;
    private ArrayList<Integer> argbColors;
    private boolean allowSplines = true;
    private File directory = null;
    private int ordinal = 0;
    private int refCount;

    private boolean isLoaded = false;

    private PolynomialSplineFunction hueFunction = null;
    private PolynomialSplineFunction saturationFunction = null;
    private PolynomialSplineFunction valueFunction = null;
    private boolean warmed = false;
    private boolean readOnly = false;
    private String name = null;

    public ColorScheme() {
        super(null);;
        hsvComponents = new ArrayList<>();
        argbColors = new ArrayList<>();
    }

    public ColorScheme(File directory) {
        this();
        this.directory = directory;

        String numStr = directory.getName().replaceFirst("^" + Pattern.quote(COLOR_SCHEMES_NAME_PREFIX), "");
        try {
            this.ordinal = Integer.parseInt(numStr);
        } catch (NumberFormatException e) {
            ordinal = -1; // TODO
        }
    }

    public ColorScheme(ColorScheme other) {
        super(other);
        this.hsvComponents = new ArrayList<>(other.hsvComponents);
        this.argbColors = new ArrayList<>(other.argbColors);
        this.allowSplines = other.allowSplines;
        this.directory = other.directory;
        this.isLoaded = other.isLoaded;
        this.ordinal = other.ordinal;
        warm();
    }

    public void addColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        argbColors.add(color);
        hsvComponents.add(hsv);
        warmed = false;
    }

    public void addColor(float[] hsv) {
        int color = Color.HSVToColor(hsv);
        argbColors.add(color);
        hsvComponents.add(hsv);
        warmed = false;
    }

    public void setColor(int index, int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        argbColors.set(index, color);
        hsvComponents.set(index, hsv);
        warmed = false;
    }

    public void setColor(int index, float[] hsv) {
        int color = Color.HSVToColor(hsv);
        argbColors.set(index, color);
        hsvComponents.set(index, hsv);
        warmed = false;
    }

    public int removeColor(int index) {
        hsvComponents.remove(index);
        warmed = false;
        return argbColors.remove(index);
    }

    public int getColor(int index) {
        return argbColors.get(index);
    }

    public float[] getColorHSV(int index) {
        return hsvComponents.get(index);
    }

    public boolean isAllowSplines() {
        return allowSplines;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public void setAllowSplines(boolean allowSplines) {
        this.allowSplines = allowSplines;
    }

    public int size() {
        return argbColors.size();
    }

    private double clampHue(double hue) {
        return hue < 0d ? 360d + (hue % 360d) : hue % 360d;
    }

    private void setupHueInputs(double[] arguments, double[] values) {
        arguments[0] = 0;
        values[0] = hsvComponents.get(0)[0];

        for (int i = 1; i < hsvComponents.size(); ++i) {
            arguments[i] = i;
            double prev = (float)values[i - 1];
            double clampPrev = clampHue(prev);
            double clampCurrent = hsvComponents.get(i)[0];

            if (clampCurrent - clampPrev > 180d) {
                clampCurrent -= 360d;
            } else if (clampPrev - clampCurrent > 180d) {
                clampCurrent += 360d;
            }
            double current = prev - (clampPrev - clampCurrent);
            values[i] = current;
        }
    }

    private void setupInputs(double[] arguments, double[] values, int hsvIndex) {
        for (int i = 0; i < hsvComponents.size(); ++i) {
            arguments[i] = i;
            values[i] = hsvComponents.get(i)[hsvIndex];
        }
    }

    private PolynomialSplineFunction getFunction(boolean isSpline, int hsvIndex) {
        double[] arguments = new double[hsvComponents.size()];
        double[] values = new double[hsvComponents.size()];

        if (hsvIndex == HUE_INDEX) {
            setupHueInputs(arguments, values);
        } else {
            setupInputs(arguments, values, hsvIndex);
        }

        if (isSpline) {
            return new SplineInterpolator().interpolate(arguments, values);
        } else {
            return new LinearInterpolator().interpolate(arguments, values);
        }
    }

    public void warm() {
        if (argbColors.size() == 2) {
            hueFunction = getFunction(false, HUE_INDEX);
            saturationFunction = getFunction(false, SATURATION_INDEX);
            valueFunction = getFunction(false, VALUE_INDEX);
        } else if (argbColors.size() > 2) {
            hueFunction = getFunction(allowSplines, HUE_INDEX);
            saturationFunction = getFunction(allowSplines, SATURATION_INDEX);
            valueFunction = getFunction(allowSplines, VALUE_INDEX);
        }
        warmed = true;
    }

    /**
     * Interpolates the color based on the given position.  Essentially the same as getColor(int)
     * but with a double argument which does interpolation in the HSV color space
     * @param position position of color.  Must be >= 0d and <= size()
     * @return the new interpolated color
     */
    public float[] getColorHSV(double position, float[] hsv) {
        if (position < 0d) {
            position = 0d;
        }
        else if (position > size() - 1) {
            position = size() - 1;
        }
        if(!warmed) {
            if (argbColors.size() == 0) {
                hsv[HUE_INDEX] = 0;
                hsv[SATURATION_INDEX] = 0;
                hsv[VALUE_INDEX] = 0;
                return hsv;
            } else if (argbColors.size() <= 1) {
                return hsvComponents.get(0);
            }
            warm();
        }

        float hue = (float) hueFunction.value(position);
        float saturation = (float) saturationFunction.value(position);
        float value = (float) valueFunction.value(position);

        hue = hue < 0 ? 360f + (hue % 360f) : hue % 360;
        saturation = Math.max(0f, Math.min(1f, saturation));
        value = Math.max(0f, Math.min(1f, value));

        hsv[0] = hue;
        hsv[1] = saturation;
        hsv[2] = value;
        return hsv;
    }

    public int getColor(double position) {
        return Color.HSVToColor(getColorHSV(position, new float[3]));
    }

    public ColorSchemePojo makeColorSchemePojo() {
        ColorSchemePojo pojo = new ColorSchemePojo();
        pojo.setHsvComponents(new ArrayList<float[]>(hsvComponents));

        ArrayList<Integer> positions = new ArrayList<>();
        for (int i = 0; i < hsvComponents.size(); ++i) {
            positions.add(i);
        }
        pojo.setPositions(positions);
        pojo.setOrdinal(ordinal);
        pojo.setAllowSplines(allowSplines);
        pojo.setRefCount(refCount);
        return pojo;
    }

    public int getRefCount() {
        return refCount;
    }

    public void setFromPojo(ColorSchemePojo pojo) {
        for (float[] hsv : pojo.getHsvComponents()) {
            addColor(hsv);
        }
        allowSplines = pojo.isAllowSplines();
        ordinal = pojo.getOrdinal();
        refCount = pojo.getRefCount();
    }

    public boolean load() {
        if (isLoaded) {
            return true;
        } else {
            if (!directory.exists()) {
                return false;
            }

            File jsonFile = new File(directory.getAbsoluteFile() + File.separator + DATA_FILE_NAME);
            if (!jsonFile.exists()) {
                return false;
            }

            Reader in = null;

            try {
                in = new BufferedReader(new FileReader(jsonFile));
                Gson gson = new Gson();
                ColorSchemePojo pojo = gson.fromJson(in, ColorSchemePojo.class);
                setFromPojo(pojo);

                isLoaded = true;
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return isLoaded;
        }
    }

    public boolean save() {
        if (!directory.exists()) {
            boolean result = directory.mkdir();
            if (!result) {
                return false;
            }
        }

        File file = new File(directory.getAbsoluteFile() + File.separator + DATA_FILE_NAME);
        PrintWriter pw = null;
        try {
            Gson gson = new Gson();
            String str = gson.toJson(makeColorSchemePojo());

            pw = new PrintWriter(file);
            pw.println(str);
            pw.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (pw != null) {
                pw.close();
            }
        }

        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void delete() throws IOException {
        FileUtils.deleteDirectory(directory);
    }

    public boolean exists() {
        return directory.exists();
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    public File getDirectory() {
        return this.directory;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("colors: [");
        for (int color : argbColors) {
            sb.append(Integer.toHexString(color)).append(" ");
        }
        sb.append("]\ndirectory: ").append(directory);
        sb.append("\nallowSplines: ").append(allowSplines);
        sb.append("\nwarmed: ").append(warmed);
        sb.append("\nisLoaded: ").append(isLoaded);

        return sb.toString();
    }

    @Override
    public int getColor(final double x, final double y, final float[] hsvBuffer) {
        int iterations = this.getIterationStrategy().getEscapeIterations(x, y);
        if (iterations >= this.getIterationStrategy().getMaximumEscapeIterations()) {
            return 0xFF000000; // black
        }

        double refinedIterations = (Math.sqrt(iterations) * hsvComponents.size()) / REFINEMENT_DIVISOR;
        return Color.HSVToColor(getColorHSV(refinedIterations % hsvComponents.size(), hsvBuffer));
    }
}
