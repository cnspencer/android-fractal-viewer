package com.charlestheman.basicfractal;

import android.app.ActivityManager;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Semaphore;

import javax.vecmath.Matrix3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

/**
 * Contains the sample data to be calculated using the provided iterators.
 * <p/>
 * Note that the methods in this class are not actually thread safe, but the iterators are.
 * Ideally, all the methods would be accessed from the UI thread and the iterators processed
 * on background threads.
 */
public class SampleManager implements CalculationIterable<SampleManager.CalcInfo, SampleManager.Result> {

    private final MyMatrix unitToSampleSpaceProjection;
    private final Matrix3d totalMatrix3d = new Matrix3d();
    private int[] sampleBuffer = null;
    private int sampleHeight = 1;
    private int sampleWidth = 1;
    private Matrix3d positionMatrix3d = new Matrix3d();
    private Semaphore modificationSemaphore = null;
    private int iteratorsCreated = -1;
    private boolean cancelled = false;

    /**
     * @param sampleWidth       Width of pixels for just this image.  Must be double to maintain correct aspect ratio
     * @param sampleHeight      Height of pixels for just this image. Must be double to maintain correct aspect ratio
     */
    public SampleManager(double sampleWidth, double sampleHeight) {
        this.sampleHeight = (int) Math.round(sampleHeight);
        this.sampleWidth = (int) Math.round(sampleWidth);

//        long availableMemory = Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory();
//        if (availableMemory - 4 < this.sampleHeight * this.sampleWidth * (Integer.SIZE / Byte.SIZE)) {
//            throw new RuntimeException("Not enough memory to instantiate SampleManager()!");
//        }

        this.sampleBuffer = new int[this.sampleHeight * this.sampleWidth];

        unitToSampleSpaceProjection = new ProjectionMatrix(sampleWidth, sampleHeight);

        calculateTotalMatrix3d();
        reset();
    }

    public void reset() {
        if (isCalculating()) {
            throw new RuntimeException("Cannot reset while calculating!");
        }
        modificationSemaphore = null;
        iteratorsCreated = -1;
        cancelled = false;
    }

    public int[] getSampleBuffer() {
        return sampleBuffer;
    }

    public int getSampleHeight() {
        return sampleHeight;
    }

    public int getSampleWidth() {
        return sampleWidth;
    }

    private void calculateTotalMatrix3d() {
        this.totalMatrix3d.mul(this.positionMatrix3d, this.unitToSampleSpaceProjection);
    }

    public Matrix3d getPositionMatrix3d() {
        return positionMatrix3d;
    }

    public void setPositionMatrix3d(final Matrix3d positionMatrix3d) {
        this.positionMatrix3d.set(positionMatrix3d);
        calculateTotalMatrix3d();
    }

    /////// code for CalculationIterable ////////////////

    /**
     * Generates a list of CalculationIterators that iterate over samples that are mutually exclusive
     * and exhaustive for all the samples that must be calculated.
     * <p/>
     * The SampleManager must be in the "ready" (isReady() == true) state, and will transition into
     * the "calculating" (isCalculating() == true) state.
     *
     * @param num The number of CalculationIterators to generate
     * @return A List of CalculationIterators
     */
    @Override
    public List<CalculationIterator<CalcInfo, Result>> iterators(int num) {
        if (num < 1) {
            throw new IllegalArgumentException("num must be at least 1!");
        }

        if (!isReady()) {
            throw new RuntimeException("iterators() has already been called!");
        }
        modificationSemaphore = new Semaphore(num);

        List<CalculationIterator<CalcInfo, Result>> list = new ArrayList<>();

        if (num > this.sampleBuffer.length) {
            num = this.sampleBuffer.length;
        }

        int minLength = this.sampleBuffer.length / num;
        int remainder = this.sampleBuffer.length % num;

        int nextIndex = 0;

        for (int i = 0; i < remainder; ++i) {
            int length = minLength + 1;
            CalculationIterator<CalcInfo, Result> ci =
                    new CalculationIteratorImpl(nextIndex, nextIndex + length - 1);
            list.add(ci);
            nextIndex += length;
        }

        for (int i = 0; i < num - remainder; ++i) {
            CalculationIterator<CalcInfo, Result> ci =
                    new CalculationIteratorImpl(nextIndex, nextIndex + minLength - 1);
            list.add(ci);
            nextIndex += minLength;
        }
        iteratorsCreated = num;
        return list;
    }

    /**
     * Returns true if ready to begin calculations.
     *
     * @return true if ready to begin calculations, false otherwise
     */
    public boolean isReady() {
        return modificationSemaphore == null;
    }

    /**
     * Returns true if currently performing calculations
     *
     * @return true if currently performing calculations, false otherwise
     */
    public boolean isCalculating() {
        if (modificationSemaphore != null) {
            if (modificationSemaphore.availablePermits() != this.iteratorsCreated) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if finished calculating and not cancelled
     *
     * @return true if finished calculating and not cancelled, false otherwise
     */
    public boolean isDone() {
        if (modificationSemaphore != null) {
            if (modificationSemaphore.availablePermits() == this.iteratorsCreated) {
                return !cancelled;
            }
        }
        return false;
    }

    /**
     * Returns true if finished calculating and not cancelled
     *
     * @return true if finished calculating and not cancelled, false otherwise
     */
    public boolean isCancelled() {
        if (modificationSemaphore != null) {
            if (modificationSemaphore.availablePermits() == this.iteratorsCreated) {
                return cancelled;
            }
        }
        return false;
    }

    public static class CalcInfo {
        public double x;
        public double y;
    }

    public static class Result {
        public int color;
        public boolean cancelled = false;
    }

    private class CalculationIteratorImpl implements CalculationIterator<CalcInfo, Result> {
        private final int lastIndex;
        private final Tuple3d currentPoint3d = new Vector3d();
        private boolean opened = true;
        private int currentIndex;

        public CalculationIteratorImpl(int firstIndex, int lastIndex) {
            this.lastIndex = lastIndex;
            this.currentIndex = firstIndex - 1; // this may make it start off as negative, but the math checks out, so DEAL WITH IT
            modificationSemaphore.acquireUninterruptibly();
        }

        @Override
        public boolean hasNext() {
            return currentIndex < lastIndex;
        }

        @Override
        public void next(CalcInfo calcInfo) {
            if (calcInfo == null) {
                throw new NullPointerException("CalcInfo cannot be null!");
            } else if (!this.hasNext()) {
                throw new NoSuchElementException("Cannot call next() when hasNext() returns false!");
            }
            ++currentIndex;
            this.currentPoint3d.x = currentIndex % sampleWidth;
            this.currentPoint3d.y = currentIndex / sampleWidth;
            this.currentPoint3d.z = 1.0d;
            totalMatrix3d.transform(this.currentPoint3d);

            calcInfo.x = this.currentPoint3d.x;
            calcInfo.y = this.currentPoint3d.y;
        }

        @Override
        public void set(Result result) {
            if (result == null) {
                throw new NullPointerException("Result cannot be null!");
            } else if (this.opened) {
                cancelled = result.cancelled;
                sampleBuffer[this.currentIndex] = result.color;
            } else {
                throw new RuntimeException("Cannot call set() if iterator is closed!");
            }
        }

        /**
         * Closes one iterator.  Note that closing the last one will implicitly transition the
         * SampleManager from "calculating" (isCalculating() == true) to "done" (isDone() == true)
         */
        @Override
        public void close() {
            if (this.opened) {
                this.opened = false;
                modificationSemaphore.release(); // deliberately allow NullPointerException
            }
        }
    }
}
