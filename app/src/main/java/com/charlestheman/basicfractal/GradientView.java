package com.charlestheman.basicfractal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Arrays;

/**
 * View to display a coloring gradient described by a ColorScheme
 */
public class GradientView extends LinearLayout {
    private static final int DEFAULT_COLOR = 0xFF000000; // TODO resource?

    private ColorScheme colorScheme = null;
    private boolean layoutDone = false;

    public GradientView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            setBackgroundDrawable(generateGradientBitmap());
        }
        layoutDone = true;
    }

    public ColorScheme getColorScheme() {
        return colorScheme;
    }

    public void setColorScheme(ColorScheme colorScheme) {
        this.colorScheme = colorScheme;
        if (layoutDone) {
            setBackgroundDrawable(generateGradientBitmap());
        }
    }

    private BitmapDrawable generateGradientBitmap() {
        if (colorScheme == null) {
            return null;
        }

        int height;
        int width;
        int length;
        if (getOrientation() == HORIZONTAL) {
            height = 1;
            width = getWidth();
            length = width;
        } else {
            height = getHeight();
            width = 1;
            length = height;
        }

        int[] colors = new int[length];
        double colorCount = colorScheme.size();
        if (colorCount <= 1) {
            Arrays.fill(colors, colorCount == 0 ? DEFAULT_COLOR : colorScheme.getColor(0));
        } else {
            double factor = (colorCount - 1) / (double) length;
            for (int i = 0; i < colors.length; ++i) {
                colors[i] = colorScheme.getColor((double) i * factor);
            }
        }

        Bitmap gradientBitmap = Bitmap.createBitmap(colors, width, height, Bitmap.Config.ARGB_8888);

        BitmapDrawable drawable = new BitmapDrawable(getResources(), gradientBitmap);
        drawable.setTileModeX(Shader.TileMode.REPEAT);
        drawable.setTileModeY(Shader.TileMode.REPEAT);

        return drawable;
    }
}
