package com.charlestheman.basicfractal;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.charlestheman.basicfractal.ColorScheme.COLOR_SCHEMES_NAME_PREFIX;

/**
 * Manages operations related to color schemes
 */
public class ColorSchemeManager {
    public static final int MAX_COLOR_SCHEMES = 200;

    private static final String COLOR_SCHEMES_DIRECTORY = "color_schemes";
    private static final String CURRENT_COLOR_SCHEME_DIRECTORY = "current_color_scheme";
    private static final String PREF_LAST_COLOR_SCHEME_NUM = "pref_last_color_scheme_num";

    private final Context baseContext;
    private File colorSchemesDirectory = null;

    public ColorSchemeManager(Context baseContext) {
        this.baseContext = baseContext;
    }

    private File getColorSchemesDirectory() {
        if (colorSchemesDirectory == null) {
            File internalDir = baseContext.getFilesDir();
            String favoritesPathname = internalDir.getAbsolutePath() +
                    File.separator + COLOR_SCHEMES_DIRECTORY + File.separator;
            File newColorSchemesDir = new File(favoritesPathname);

            if (!newColorSchemesDir.exists()) {
                boolean result = newColorSchemesDir.mkdir();
                if (!result) {
                    Log.e(ColorSchemeManager.class.getName(), "Color Schemes directory can't be created!");
                } else {
                    colorSchemesDirectory = newColorSchemesDir;
                }
            } else {
                colorSchemesDirectory = newColorSchemesDir;
            }
        }

        return colorSchemesDirectory;
    }

    private File getNextColorSchemeDir() {
        File colorSchemesDir = getColorSchemesDirectory();
        if (colorSchemesDir == null) return null;

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(baseContext);
        int lastColorSchemeNum = sharedPref.getInt(PREF_LAST_COLOR_SCHEME_NUM, 0);

        File[] subDirs = colorSchemesDir.listFiles();
        int nextNum = lastColorSchemeNum + 1;

        outer:
        for (int i = 0; i <= MAX_COLOR_SCHEMES; ++i, nextNum = (nextNum + 1) % MAX_COLOR_SCHEMES){
            for (File file : subDirs) {
                if (file.isDirectory() && file.getName().equals(COLOR_SCHEMES_NAME_PREFIX + nextNum)) {
                    continue outer; // try the next name
                }
            }
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(PREF_LAST_COLOR_SCHEME_NUM, nextNum);
            editor.apply();
            return new File(colorSchemesDir.getAbsolutePath() + File.separator + COLOR_SCHEMES_NAME_PREFIX + nextNum);
        }
        return null;
    }

    public int countColorSchemes() {
        return getColorSchemesDirectory().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        }).length;
    }

    public boolean canAddColorScheme() {
        return countColorSchemes() < MAX_COLOR_SCHEMES;
    }

    public List<ColorScheme> getAllColorSchemes() {
        List<ColorScheme> colorSchemes = new ArrayList<>();
        for (File file : getColorSchemesDirectory().listFiles()) {
            // assuming that the file for a favorite
            if (file.isDirectory() && file.getName().startsWith(ColorScheme.COLOR_SCHEMES_NAME_PREFIX)) {
                ColorScheme colorScheme = new ColorScheme(file);
                colorScheme.load();
                colorSchemes.add(colorScheme);
            }
        }
        Collections.sort(colorSchemes, new Comparator<ColorScheme>() {
            @Override
            public int compare(ColorScheme lhs, ColorScheme rhs) {
                return lhs.getOrdinal() - rhs.getOrdinal();
            }
        });
        Collections.reverse(colorSchemes);
        return colorSchemes;
    }

    public ColorScheme retrieveColorScheme(String directoryName) {
        File file = new File(this.getColorSchemesDirectory() + File.separator + directoryName);
        if (file.exists()) {
            ColorScheme colorScheme = new ColorScheme(file);
            return colorScheme.load() ? colorScheme : null;
        } else {
            return null;
        }
    }

    public ColorScheme createColorScheme() {
        File colorSchemeDir = getNextColorSchemeDir();
        if (colorSchemeDir == null) {
            return null;
        }
        return new ColorScheme(colorSchemeDir);
    }

    public void saveColorSchemeAsCurrent(ColorScheme colorScheme) {
        ColorScheme copy = new ColorScheme(colorScheme);
        copy.setDirectory(new File(getColorSchemesDirectory() + File.separator + CURRENT_COLOR_SCHEME_DIRECTORY));
        copy.save();
    }

    public ColorScheme getCurrentColorScheme() {
        ColorScheme colorScheme = retrieveColorScheme(CURRENT_COLOR_SCHEME_DIRECTORY);
        colorScheme.load();
        return colorScheme;
    }
}
